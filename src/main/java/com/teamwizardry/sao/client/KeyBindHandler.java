package com.teamwizardry.sao.client;

import com.teamwizardry.librarianlib.features.network.PacketHandler;
import com.teamwizardry.sao.SAO;
import com.teamwizardry.sao.common.network.PacketOpenGUI;
import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@Mod.EventBusSubscriber(modid = SAO.MOD_ID, value = Side.CLIENT)
public class KeyBindHandler {

	@SideOnly(Side.CLIENT)
	@SubscribeEvent
	public static void onEvent(InputEvent.KeyInputEvent event) {
		if (Minecraft.getMinecraft().player == null) return;

		KeyBinding binding = Keybinds.keyG;

		if (binding.isPressed()) {
			PacketHandler.NETWORK.sendToServer(new PacketOpenGUI());
		}
	}
}
