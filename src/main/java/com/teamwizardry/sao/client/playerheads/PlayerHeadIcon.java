package com.teamwizardry.sao.client.playerheads;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.teamwizardry.sao.api.util.DataReader;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.*;
import net.minecraft.client.renderer.texture.ITextureObject;
import net.minecraft.client.renderer.texture.SimpleTexture;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.renderer.texture.TextureUtil;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.resources.DefaultPlayerSkin;
import net.minecraft.client.resources.IResourceManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.lwjgl.opengl.GL11;

import javax.annotation.Nullable;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.UUID;

/**
 * @author LatvianModder
 * Stolen with permission from FTBLib
 */
public class PlayerHeadIcon
{
    private static class ThreadLoadSkin extends SimpleTexture
    {
        private final PlayerHeadIcon icon;
        private final IImageBuffer imageBuffer;
        private BufferedImage bufferedImage;
        private Thread imageThread;
        private boolean textureUploaded;

        public ThreadLoadSkin(PlayerHeadIcon i)
        {
            super(DefaultPlayerSkin.getDefaultSkin(i.uuid));
            icon = i;
            imageBuffer = new ImageBufferDownload();
        }

        @Override
        public int getGlTextureId()
        {
            if (!textureUploaded)
            {
                if (bufferedImage != null)
                {
                    deleteGlTexture();
                    TextureUtil.uploadTextureImage(super.getGlTextureId(), bufferedImage);
                    textureUploaded = true;
                }
            }

            return super.getGlTextureId();
        }

        @Override
        public void loadTexture(IResourceManager resourceManager) throws IOException
        {
            if (bufferedImage == null)
            {
                super.loadTexture(resourceManager);
            }

            if (imageThread == null)
            {
                imageThread = new Thread("Skin Downloader for " + icon.uuid)
                {



                    @Override
                    public void run()
                    {
                        String imageUrl = "";

                        try
                        {
                            String jsonType = "application/json; charset=utf-8";
                            JsonObject json = DataReader.get(new URL("https://sessionserver.mojang.com/session/minecraft/profile/" + fromUUID(icon.uuid)), jsonType, Minecraft.getMinecraft().getProxy()).json().getAsJsonObject();

                            for (JsonElement element : json.get("properties").getAsJsonArray())
                            {
                                if (element.isJsonObject() && element.getAsJsonObject().get("name").getAsString().equals("textures"))
                                {
                                    String base64 = new String(Base64.getDecoder().decode(element.getAsJsonObject().get("value").getAsString()), StandardCharsets.UTF_8);
                                    JsonObject json1 = DataReader.get(base64).json().getAsJsonObject().get("textures").getAsJsonObject();

                                    if (json1.has("SKIN"))
                                    {
                                        imageUrl = json1.get("SKIN").getAsJsonObject().get("url").getAsString();
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                        }

                        if (imageUrl.isEmpty())
                        {
                            return;
                        }

                        try
                        {
                            bufferedImage = imageBuffer.parseUserSkin(DataReader.get(new URL(imageUrl), "image/png", Minecraft.getMinecraft().getProxy()).image());
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                };

                imageThread.setDaemon(true);
                imageThread.start();
            }
        }
    }

    public final UUID uuid;

    public PlayerHeadIcon(@Nullable UUID id)
    {
        texture = id == null ? DefaultPlayerSkin.getDefaultSkinLegacy() : new ResourceLocation("skins/" + fromUUID(id));
        uuid = id;
    }

    @SideOnly(Side.CLIENT)
    public void bindTexture()
    {
        TextureManager manager = Minecraft.getMinecraft().getTextureManager();
        ITextureObject img = manager.getTexture(texture);

        if (img == null)
        {
            img = new ThreadLoadSkin(this);
            manager.loadTexture(texture, img);
        }

        GlStateManager.bindTexture(img.getGlTextureId());
    }

    private static void digitsUUID(StringBuilder sb, long val, int digits)
    {
        long hi = 1L << (digits * 4);
        String s = Long.toHexString(hi | (val & (hi - 1)));
        sb.append(s, 1, s.length());
    }

    private static String fromUUID(@Nullable UUID id)
    {
        if (id != null)
        {
            long msb = id.getMostSignificantBits();
            long lsb = id.getLeastSignificantBits();
            StringBuilder sb = new StringBuilder(32);
            digitsUUID(sb, msb >> 32, 8);
            digitsUUID(sb, msb >> 16, 4);
            digitsUUID(sb, msb, 4);
            digitsUUID(sb, lsb >> 48, 4);
            digitsUUID(sb, lsb, 12);
            return sb.toString();
        }

        return "";
    }

    @SideOnly(Side.CLIENT)
    public void draw(int x, int y, int w, int h)
    {
        bindTexture();
        drawTexturedRect(x, y, w, h, 0.125D, 0.125D, 0.25D, 0.25D);
        drawTexturedRect(x, y, w, h, 0.625D, 0.125D, 0.75D, 0.25D);
    }

    public static void drawTexturedRect(int x, int y, int w, int h, double u0, double v0, double u1, double v1)
    {

        if (u0 == u1 || v0 == v1)
        {
            Tessellator tessellator = Tessellator.getInstance();
            BufferBuilder buffer = tessellator.getBuffer();
            buffer.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_COLOR);
            addRectToBuffer(buffer, x, y, w, h);
            tessellator.draw();
        }
        else
        {
            Tessellator tessellator = Tessellator.getInstance();
            BufferBuilder buffer = tessellator.getBuffer();
            buffer.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX_COLOR);
            addRectToBufferWithUV(buffer, x, y, w, h, u0, v0, u1, v1);
            tessellator.draw();
        }
    }

    public static void addRectToBuffer(BufferBuilder buffer, int x, int y, int w, int h)
    {
        int r = 255;
        int g = 255;
        int b = 255;
        int a = 255;
        buffer.pos(x, y + h, 0D).color(r, g, b, a).endVertex();
        buffer.pos(x + w, y + h, 0D).color(r, g, b, a).endVertex();
        buffer.pos(x + w, y, 0D).color(r, g, b, a).endVertex();
        buffer.pos(x, y, 0D).color(r, g, b, a).endVertex();
    }

    public static void addRectToBufferWithUV(BufferBuilder buffer, int x, int y, int w, int h, double u0, double v0, double u1, double v1)
    {
        int r = 255;
        int g = 255;
        int b = 255;
        int a = 255;
        buffer.pos(x, y + h, 0D).tex(u0, v1).color(r, g, b, a).endVertex();
        buffer.pos(x + w, y + h, 0D).tex(u1, v1).color(r, g, b, a).endVertex();
        buffer.pos(x + w, y, 0D).tex(u1, v0).color(r, g, b, a).endVertex();
        buffer.pos(x, y, 0D).tex(u0, v0).color(r, g, b, a).endVertex();
    }

    @Override
    public String toString()
    {
        return "player:" + uuid;
    }

    public final ResourceLocation texture;
}
