package com.teamwizardry.sao.client;

import net.minecraft.client.settings.KeyBinding;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.lwjgl.input.Keyboard;

@SideOnly(Side.CLIENT)
public class Keybinds {

	public static KeyBinding keyG;

	public static void register() {
		keyG = new KeyBinding("key.g.desc", Keyboard.KEY_G, "key.categoryButtons.misc");
	}
}
