package com.teamwizardry.sao.client.gui

import com.teamwizardry.librarianlib.features.gui.component.GuiComponent
import com.teamwizardry.librarianlib.features.gui.component.GuiComponentEvents
import com.teamwizardry.librarianlib.features.gui.components.ComponentRect
import com.teamwizardry.librarianlib.features.gui.components.ComponentSprite
import com.teamwizardry.librarianlib.features.gui.components.ComponentTextField
import com.teamwizardry.librarianlib.features.gui.layers.TextLayer
import com.teamwizardry.librarianlib.features.helpers.vec
import com.teamwizardry.librarianlib.features.kotlin.Minecraft
import com.teamwizardry.librarianlib.features.math.Align2d
import com.teamwizardry.librarianlib.features.math.Vec2d
import com.teamwizardry.librarianlib.features.network.PacketHandler
import com.teamwizardry.librarianlib.features.sprite.Sprite
import com.teamwizardry.sao.SAO
import com.teamwizardry.sao.api.DataGuild
import com.teamwizardry.sao.client.gui.GuiSAOUI.Companion.getGuildContainingMember
import com.teamwizardry.sao.client.gui.GuiSAOUI.Companion.getGuildsInvitingMember
import com.teamwizardry.sao.client.gui.GuiSAOUI.Companion.getPlayerData
import com.teamwizardry.sao.common.network.guild.*
import net.minecraft.util.ResourceLocation
import java.awt.Color
import kotlin.math.max
import kotlin.math.min

class ComponentGuildManagement(width: Int, height: Int, val gui: GuiSAOUI) : ComponentWindow(width, height, EnumWindowSide.RIGHT) {

    companion object {
        private val iconFriend = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/friend.png"))
        private val iconFriendSelected = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/friend_activated.png"))

        private val iconPlus = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/plus.png"))
        private val iconPlusSelected = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/plus_activated.png"))

        private val iconInvite = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/invite.png"))
        private val iconInviteSelected = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/invite_activated.png"))

        private val iconNo = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/no.png"))
        private val iconNoSelected = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/no_activated.png"))

        private val iconYes = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/yes.png"))
        private val iconYesSelected = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/yes_activated.png"))
    }

    private var primarySlide: ComponentSlide? = null

    init {

        val currentGuild = getGuildContainingMember()
        val guildInvites = getGuildsInvitingMember()

        if (guildInvites.isNotEmpty()) {
            val slideGuildInvites = ComponentSlide(this, EnumWindowSide.LEFT)
            primarySlide = slideGuildInvites
            addSlide(slideGuildInvites)

            val guildName = TextLayer(0, 2, slideGuildInvites.widthi, 22)
            guildName.text = "Guild Invites:"
            guildName.align = Align2d.CENTER
            slideGuildInvites.addToBase(guildName)

            val scrollable = GuiComponent(0, 2 + 22 + 2, slideGuildInvites.widthi, 5 * 20 + 5 * 2)
            scrollable.clipToBounds = true

            scrollable.BUS.hook(GuiComponentEvents.MouseWheelEvent::class.java) { it3 ->
                val capacity = scrollable.children.size - 5
                when (it3.direction) {
                    GuiComponentEvents.MouseWheelDirection.UP -> {
                        scrollable.contentsOffset = vec(0, min(0.0, scrollable.contentsOffset.y + 22))
                    }
                    GuiComponentEvents.MouseWheelDirection.DOWN -> {
                        scrollable.contentsOffset = vec(0, max(-capacity * 20.0 + -capacity * 2.0, scrollable.contentsOffset.y - 22))
                    }
                }
            }
            slideGuildInvites.addToBase(scrollable)

            var i = 0
            for (j in 0..10)
                for (invite in guildInvites) {
                    val rectInvite = ComponentRect(0, i * 20 + i * 2, scrollable.widthi, 20)
                    rectInvite.color = Color(255, 244, 178)

                    i++

                    val memberIcon = ComponentSprite(iconPlus, 2, 2, 16, 16)
                    rectInvite.add(memberIcon)

                    val accept = ComponentSprite(iconYes, rectInvite.widthi - 16 - 16 - 8, 2, 16, 16)
                    accept.BUS.hook(GuiComponentEvents.MouseMoveInEvent::class.java) {
                        accept.sprite = iconYesSelected
                    }
                    accept.BUS.hook(GuiComponentEvents.MouseMoveOutEvent::class.java) {
                        accept.sprite = iconYes
                    }
                    accept.BUS.hook(GuiComponentEvents.MouseClickEvent::class.java) {
                        PacketHandler.NETWORK.sendToServer(PacketAcceptGuildInvite(invite.guildUUID))
                    }
                    rectInvite.add(accept)

                    val deny = ComponentSprite(iconNo, rectInvite.widthi - 16 - 4, 2, 16, 16)
                    deny.BUS.hook(GuiComponentEvents.MouseMoveInEvent::class.java) {
                        deny.sprite = iconNoSelected
                    }
                    deny.BUS.hook(GuiComponentEvents.MouseMoveOutEvent::class.java) {
                        deny.sprite = iconNo
                    }
                    deny.BUS.hook(GuiComponentEvents.MouseClickEvent::class.java) {
                        PacketHandler.NETWORK.sendToServer(PacketRemoveGuildInvite(invite.guildUUID))
                    }
                    rectInvite.add(deny)

                    scrollable.add(rectInvite)

                    val nameText = TextLayer(20, 0, rectInvite.widthi - 2, 20)
                    nameText.text = "${invite.name}"
                    nameText.truncate = true
                    nameText.align = Align2d.LEFT_CENTER
                    nameText.maxLines = 2
                    nameText.lineSpacing = 1
                    rectInvite.add(nameText)

                    scrollable.add(rectInvite)

                }
            slideGuildInvites.wipeIn()

        } else if (currentGuild == null) {
            val slideGuildName = ComponentSlide(this, EnumWindowSide.LEFT)
            primarySlide = slideGuildName
            addSlide(slideGuildName)

            val notifText = TextLayer(0, 2, slideGuildName.widthi, 32)
            notifText.text = "Create a Guild.\nEnter your guild's name:"
            notifText.align = Align2d.CENTER
            notifText.maxLines = 2
            notifText.lineSpacing = 1
            slideGuildName.addToBase(notifText)

            val fieldRect = ComponentRect(0, 32, slideGuildName.widthi, 13)
            fieldRect.color = Color(255, 255, 255)
            slideGuildName.addToBase(fieldRect)

            val field = ComponentTextField(Minecraft().fontRenderer, 0, 32 + 2, slideGuildName.widthi, 11)
            field.isFocused = true
            field.canLoseFocus = false
            field.useShadow = false

            field.enabledColor = Color.BLACK
            field.cursorColor = Color.BLACK
            field.disabledColor = Color.GRAY
            field.selectionColor = Color.GRAY
            field.maxStringLength = 16
            slideGuildName.addToBase(field)

            val accept = ComponentSprite(iconYes, (slideGuildName.widthi / 2) - (16 / 2), 32 + 16, 16, 16)
            slideGuildName.addToBase(accept)
            accept.BUS.hook(GuiComponentEvents.MouseMoveInEvent::class.java) {
                accept.sprite = iconYesSelected
            }
            accept.BUS.hook(GuiComponentEvents.MouseMoveOutEvent::class.java) {
                accept.sprite = iconYes
            }
            accept.BUS.hook(GuiComponentEvents.MouseClickEvent::class.java) {
                if (field.text.trim().isEmpty()) return@hook

                val newGuild = DataGuild(Minecraft().player.uniqueID)

                val slideAddMembers = ComponentSlide(this, EnumWindowSide.LEFT)
                primarySlide = slideAddMembers
                addSlide(slideAddMembers)

                val back = ComponentSprite(triangleLeft, 4, 4, 8, 8)
                back.BUS.hook(GuiComponentEvents.MouseClickEvent::class.java) {
                    slideGuildName.wipeIn()
                }
                slideAddMembers.addToBase(back)

                val guildName = TextLayer(0, 2, slideGuildName.widthi, 22)
                guildName.text = "${field.text.trim()}\nAdd Members:"
                guildName.align = Align2d.CENTER
                slideAddMembers.addToBase(guildName)

                val scrollable = GuiComponent(0, 2 + 22 + 2, slideGuildName.widthi, 7 * 20 + 7 * 2)
                scrollable.clipToBounds = true

                scrollable.BUS.hook(GuiComponentEvents.MouseWheelEvent::class.java) { it3 ->
                    val capacity = scrollable.children.size - 7
                    when (it3.direction) {
                        GuiComponentEvents.MouseWheelDirection.UP -> {
                            scrollable.contentsOffset = vec(0, min(0.0, scrollable.contentsOffset.y + 22))
                        }
                        GuiComponentEvents.MouseWheelDirection.DOWN -> {
                            scrollable.contentsOffset = vec(0, max(-capacity * 20.0 + -capacity * 2.0, scrollable.contentsOffset.y - 22))
                        }
                    }
                }
                slideAddMembers.addToBase(scrollable)

                val data = getPlayerData(Minecraft().player.uniqueID)
                var i = 0
                if (data != null)
                for (j in 0..10)
                    for (friendData in data.friends.listFriends) {

                        val friendGuild = getGuildContainingMember(friendData.uuid)
                        val friendGuildInvites = getGuildsInvitingMember(friendData.uuid)

                        val rectFriend = ComponentRect(0, i * 20 + i * 2, scrollable.widthi, 20)
                        if (friendGuild != newGuild || friendGuildInvites.contains(newGuild)) {
                            rectFriend.color = Color(55, 55, 55)
                        } else {
                            rectFriend.color = Color(150, 255, 150)
                        }
                        i++

                        val friendIcon = ComponentSprite(iconPlus, 2, 2, 16, 16)
                        friendIcon.BUS.hook(GuiComponentEvents.MouseMoveInEvent::class.java) {
                            if (friendGuildInvites.contains(newGuild) || friendGuild?.guildUUID == newGuild.guildUUID) {
                                friendIcon.sprite = iconNo
                            } else if (friendGuildInvites.contains(newGuild)) {
                                friendIcon.sprite = iconNoSelected
                            } else {
                                friendIcon.sprite = iconPlusSelected
                            }
                        }
                        friendIcon.BUS.hook(GuiComponentEvents.MouseMoveOutEvent::class.java) {
                            if (friendGuildInvites.contains(newGuild) || friendGuild?.guildUUID == newGuild.guildUUID) {
                                friendIcon.sprite = iconNo
                            } else if (friendGuildInvites.contains(newGuild)) {
                                friendIcon.sprite = iconNo
                            } else {
                                friendIcon.sprite = iconPlusSelected
                            }
                        }

                        friendIcon.BUS.hook(GuiComponentEvents.MouseClickEvent::class.java) {
                            if (friendGuildInvites.contains(newGuild)) {
                                PacketHandler.NETWORK.sendToServer(PacketRedactGuildInvite(friendData.uuid))
                            } else if (friendGuild?.guildUUID == newGuild.guildUUID) PacketHandler.NETWORK.sendToServer(PacketSendGuildInvite(friendData.uuid, field.text.trim()))
                        }
                        rectFriend.add(friendIcon)

                        val nameText = TextLayer(20, 0, rectFriend.widthi - 2, 20)
                        nameText.text = "${friendData.name} - $i"
                        nameText.truncate = true
                        nameText.align = Align2d.LEFT_CENTER
                        nameText.maxLines = 2
                        nameText.lineSpacing = 1
                        rectFriend.add(nameText)

                        scrollable.add(rectFriend)
                    }

                slideAddMembers.wipeIn()
            }

            slideGuildName.wipeIn()
        } else if (!currentGuild.isLeader(Minecraft().player.uniqueID)) {

            val slideGuildView = ComponentSlide(this, EnumWindowSide.LEFT)
            primarySlide = slideGuildView
            addSlide(slideGuildView)

            val guildName = TextLayer(2, 2, slideGuildView.widthi - 4, 22)
            guildName.text = "${currentGuild.name}\nGuild Members:"
            guildName.align = Align2d.CENTER
            slideGuildView.addToBase(guildName)

            val scrollable = GuiComponent(2, 2 + 22 + 2, slideGuildView.widthi - 4, 5 * 20 + 5 * 2)
            scrollable.clipToBounds = true

            scrollable.BUS.hook(GuiComponentEvents.MouseWheelEvent::class.java) { it3 ->
                val capacity = scrollable.children.size - 5
                when (it3.direction) {
                    GuiComponentEvents.MouseWheelDirection.UP -> {
                        scrollable.contentsOffset = vec(0, min(0.0, scrollable.contentsOffset.y + 22))
                    }
                    GuiComponentEvents.MouseWheelDirection.DOWN -> {
                        scrollable.contentsOffset = vec(0, max(-capacity * 20.0 + -capacity * 2.0, scrollable.contentsOffset.y - 22))
                    }
                }
            }
            slideGuildView.addToBase(scrollable)

            var i = 0
            for (j in 0..10)
                for (memberUUID in currentGuild.members) {
                    val member = getPlayerData(memberUUID) ?: continue

                    val rectMember = ComponentRect(0, i * 20 + i * 2, scrollable.widthi, 20)
                    rectMember.color = Color(255, 244, 178)

                    i++

                    val memberIcon = ComponentSprite(iconPlus, 2, 2, 16, 16)
                    memberIcon.BUS.hook(GuiComponentEvents.MouseMoveInEvent::class.java) {
                        // TODO: has friend
                    }
                    memberIcon.BUS.hook(GuiComponentEvents.MouseMoveOutEvent::class.java) {
                        // TODO: has friend
                    }

                    memberIcon.BUS.hook(GuiComponentEvents.MouseClickEvent::class.java) {
                        // TODO: add friend
                    }
                    rectMember.add(memberIcon)

                    val nameText = TextLayer(20, 0, rectMember.widthi - 2, 20)
                    nameText.text = member.name + " - $i"
                    nameText.truncate = true
                    nameText.align = Align2d.LEFT_CENTER
                    nameText.maxLines = 2
                    nameText.lineSpacing = 1
                    rectMember.add(nameText)

                    scrollable.add(rectMember)
                }

            val deny = ComponentSprite(iconNo, 2, 2, 16, 16)
            deny.BUS.hook(GuiComponentEvents.MouseMoveInEvent::class.java) {
                deny.sprite = iconNoSelected
            }
            deny.BUS.hook(GuiComponentEvents.MouseMoveOutEvent::class.java) {
                deny.sprite = iconNo
            }

            deny.BUS.hook(GuiComponentEvents.MouseClickEvent::class.java) {
                PacketHandler.NETWORK.sendToServer(PacketLeaveGuild(currentGuild.guildUUID))
            }
            slideGuildView.addToBase(deny)

            val notifText = TextLayer(16 + 4, 2, slideGuildView.widthi - 16 - 2, 16)
            notifText.text = "Leave Guild"
            notifText.align = Align2d.LEFT_CENTER
            slideGuildView.addToBase(notifText)
        }
    }

    override fun wipeIn(): Float {
        if (parentSpace != null) {
            val origin = gui.categoryButtons[0]?.convertPointTo(Vec2d.ZERO, parentSpace) ?: Vec2d.ZERO
            val newPos = Vec2d(origin.x - 5 - width, origin.y + ((gui.categoryCount * 32) / 2.0) - (height / 2.0))
            pos = newPos
        }

        primarySlide?.wipeIn()
        return super.wipeIn()
    }
}