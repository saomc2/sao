package com.teamwizardry.sao.client.gui

import com.teamwizardry.librarianlib.features.gui.component.GuiComponent

interface ISwappableHolder {

    fun getCurrentOpenComponent() : ISlidableComponent?

    fun setCurrentOpenComponent(component: ISlidableComponent?)

    fun getTotalWidth() : Int

    fun getMenuHolder(): GuiComponent
}