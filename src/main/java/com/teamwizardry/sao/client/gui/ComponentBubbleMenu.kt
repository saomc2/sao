package com.teamwizardry.sao.client.gui

import com.teamwizardry.librarianlib.features.animator.Easing
import com.teamwizardry.librarianlib.features.animator.animations.BasicAnimation
import com.teamwizardry.librarianlib.features.animator.animations.ScheduledEventAnimation
import com.teamwizardry.librarianlib.features.gui.component.GuiComponent
import com.teamwizardry.librarianlib.features.gui.component.GuiComponentEvents
import com.teamwizardry.librarianlib.features.gui.components.ComponentSprite
import com.teamwizardry.librarianlib.features.gui.layers.ArcLayer
import com.teamwizardry.librarianlib.features.helpers.vec
import com.teamwizardry.librarianlib.features.kotlin.Minecraft
import com.teamwizardry.librarianlib.features.math.Vec2d
import com.teamwizardry.librarianlib.features.sprite.Sprite
import com.teamwizardry.sao.SAO
import com.teamwizardry.sao.api.capability.player.SAOPlayerCap
import net.minecraft.item.ItemStack
import net.minecraft.util.ResourceLocation
import net.minecraft.util.math.MathHelper
import java.awt.Color
import java.util.*
import kotlin.math.max


class ComponentBubbleMenu(val slot: ComponentInventory.ComponentItemSlot) : GuiComponent(0, 0), ISlidableComponent {

    private val iconDelete = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/canceldisolve.png"))
    private val iconDeleteSelected = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/canceldisolve_activated.png"))

    private val iconInvite = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/invite.png"))
    private val iconInviteSelected = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/invite_activated.png"))

    private val iconEquipment = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/equipment.png"))
    private val iconEquipmentSelected = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/equipment_selected.png"))

    private val circleComp = ArcLayer(Color.WHITE, 32 / 2, 32 / 2, 0, 0)
    private val circleCompOutline = ArcLayer(Color.WHITE, 32 / 2, 32 / 2, 0, 0)
    private val options = ArrayList<GuiComponent>()
    private val bubbleOutlineSize = 66
    private val bubbleSize = 64
    private val optionRadius = 20

    init {
        isVisible = false

        add(circleCompOutline.componentWrapper())
        circleCompOutline.color = Color(0.5f, 0.5f, 0.5f)
        circleCompOutline.anchor = vec(0, 0)

        add(circleComp.componentWrapper())
        circleComp.anchor = vec(0, 0)

        val delete = ComponentSprite(iconDelete, 32 / 2, 32 / 2, 0, 0)
        delete.isOpaqueToMouse = true

        delete.tooltip = listOf("Delete")
        delete.BUS.hook(GuiComponentEvents.MouseMoveInEvent::class.java) {
            delete.sprite = iconDeleteSelected
        }
        delete.BUS.hook(GuiComponentEvents.MouseMoveOutEvent::class.java) {
            delete.sprite = iconDelete
        }
        delete.BUS.hook(GuiComponentEvents.MouseClickEvent::class.java) {
            slot.setStack(ItemStack.EMPTY, SAOPlayerCap.get(Minecraft().player))
            wipeOut()
        }

        val equip = ComponentSprite(iconEquipment, 32 / 2, 32 / 2, 0, 0)
        equip.isOpaqueToMouse = true

        equip.tooltip = listOf("Equip")
        equip.BUS.hook(GuiComponentEvents.MouseMoveInEvent::class.java) {
            equip.sprite = iconEquipmentSelected
        }
        equip.BUS.hook(GuiComponentEvents.MouseMoveOutEvent::class.java) {
            equip.sprite = iconEquipment
        }
        equip.BUS.hook(GuiComponentEvents.MouseClickEvent::class.java) {
            wipeOut()
        }

        val share = ComponentSprite(iconInvite, 32 / 2, 32 / 2, 0, 0)
        share.isOpaqueToMouse = true

        share.tooltip = listOf("Share")
        share.BUS.hook(GuiComponentEvents.MouseMoveInEvent::class.java) {
            share.sprite = iconInviteSelected
        }
        share.BUS.hook(GuiComponentEvents.MouseMoveOutEvent::class.java) {
            share.sprite = iconInvite
        }
        share.BUS.hook(GuiComponentEvents.MouseClickEvent::class.java) {
            wipeOut()
        }

        options.add(equip)
        options.add(delete)
        options.add(share)
        add(equip, delete, share)
    }

    override fun wipeIn(): Float {
        isVisible = true

        val animExpandBubble: BasicAnimation<ArcLayer> = BasicAnimation(circleComp, "size")
        animExpandBubble.duration = 10f
        animExpandBubble.easing = Easing.easeOutCubic
        animExpandBubble.to = vec(bubbleSize, bubbleSize)
        add(animExpandBubble)

        val animMoveBubble: BasicAnimation<ArcLayer> = BasicAnimation(circleComp, "pos")
        animMoveBubble.duration = 10f
        animMoveBubble.easing = Easing.easeOutCubic
        animMoveBubble.to = vec((-bubbleSize / 2.0) + (32.0 / 2.0), (-bubbleSize / 2.0) + (32.0 / 2.0))
        add(animMoveBubble)

        val animExpandOutline: BasicAnimation<ArcLayer> = BasicAnimation(circleCompOutline, "size")
        animExpandOutline.duration = 13f
        animExpandOutline.easing = Easing.easeOutCubic
        animExpandOutline.to = vec(bubbleOutlineSize, bubbleOutlineSize)
        add(animExpandOutline)

        val animMoveOutline: BasicAnimation<ArcLayer> = BasicAnimation(circleCompOutline, "pos")
        animMoveOutline.duration = 13f
        animMoveOutline.easing = Easing.easeOutCubic
        animMoveOutline.to = vec((-bubbleOutlineSize / 2.0) + (32.0 / 2.0), (-bubbleOutlineSize / 2.0) + (32.0 / 2.0))
        add(animMoveOutline)

        val iconSize = 16

        for ((i, bubble) in options.withIndex()) {

            val radius = optionRadius
            val theta = Math.PI * 2.0 * (i.toDouble() / options.size.toDouble())
            val x = (32 / 2) - (iconSize / 2) + (radius * MathHelper.cos(theta.toFloat()))
            val z = (32 / 2) - (iconSize / 2) + (radius * MathHelper.sin(theta.toFloat()))

            val animMoveOption: BasicAnimation<GuiComponent> = BasicAnimation(bubble, "pos")
            animMoveOption.duration = i * 3f
            animMoveOption.easing = Easing.easeOutCubic
            animMoveOption.to = vec(x, z)

            val animExpandOption: BasicAnimation<GuiComponent> = BasicAnimation(bubble, "size")
            animExpandOption.duration = i * 3f
            animExpandOption.easing = Easing.easeOutCubic
            animExpandOption.to = vec(iconSize, iconSize)

            add(animExpandOption, animMoveOption)
        }


        return 10f
    }

    override fun wipeOut(): Float {
        val animEnd = ScheduledEventAnimation(max(options.size * 3f, 10f)) {
            isVisible = false
        }
        add(animEnd)

        for ((i, bubble) in options.withIndex()) {
            val animShrinkOption: BasicAnimation<GuiComponent> = BasicAnimation(bubble, "size")
            animShrinkOption.duration = (i * 3f)
            animShrinkOption.easing = Easing.easeOutCubic
            animShrinkOption.to = Vec2d.ZERO

            val animMoveOption: BasicAnimation<GuiComponent> = BasicAnimation(bubble, "pos")
            animMoveOption.duration = (i * 3f)
            animMoveOption.easing = Easing.easeOutCubic
            animMoveOption.to = vec(32.0 / 2.0, 32.0 / 2.0)
            add(animMoveOption, animShrinkOption)
        }

        val animShrinkOutline: BasicAnimation<ArcLayer> = BasicAnimation(circleCompOutline, "size")
        animShrinkOutline.duration = 10f
        animShrinkOutline.easing = Easing.easeOutCubic
        animShrinkOutline.to = Vec2d.ZERO
        add(animShrinkOutline)

        val animMoveOutline: BasicAnimation<ArcLayer> = BasicAnimation(circleCompOutline, "pos")
        animMoveOutline.duration = 10f
        animMoveOutline.easing = Easing.easeOutCubic
        animMoveOutline.to = vec((32.0 / 2.0), (32.0 / 2.0))
        add(animMoveOutline)

        val animShrinkBubble: BasicAnimation<ArcLayer> = BasicAnimation(circleComp, "size")
        animShrinkBubble.duration = 5f
        animShrinkBubble.easing = Easing.easeOutCubic
        animShrinkBubble.to = Vec2d.ZERO
        add(animShrinkBubble)

        val animMoveBubble: BasicAnimation<ArcLayer> = BasicAnimation(circleComp, "pos")
        animMoveBubble.duration = 5f
        animMoveBubble.easing = Easing.easeOutCubic
        animMoveBubble.to = vec((32.0 / 2.0), (32.0 / 2.0))
        add(animMoveBubble)

        return 10f
    }
}