package com.teamwizardry.sao.client.gui

import com.teamwizardry.librarianlib.features.gui.component.GuiComponent
import com.teamwizardry.librarianlib.features.gui.component.GuiComponentEvents
import com.teamwizardry.librarianlib.features.gui.components.ComponentRect
import com.teamwizardry.librarianlib.features.gui.components.ComponentSprite
import com.teamwizardry.librarianlib.features.gui.components.ComponentText
import com.teamwizardry.librarianlib.features.gui.components.ComponentTextField
import com.teamwizardry.librarianlib.features.gui.layers.TextLayer
import com.teamwizardry.librarianlib.features.helpers.vec
import com.teamwizardry.librarianlib.features.kotlin.Minecraft
import com.teamwizardry.librarianlib.features.math.Align2d
import com.teamwizardry.librarianlib.features.math.Vec2d
import com.teamwizardry.librarianlib.features.sprite.Sprite
import com.teamwizardry.sao.SAO
import com.teamwizardry.sao.api.util.Utils
import net.minecraft.client.Minecraft
import net.minecraft.util.ResourceLocation
import java.awt.Color
import kotlin.math.max
import kotlin.math.min

class ComponentDuelRequest(val gui: GuiSAOUI) : ComponentWindow(150, 200, EnumWindowSide.RIGHT) {


    private val iconFriend = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/friend.png"))
    private val iconFriendSelected = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/friend_activated.png"))

    private val iconNo = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/no.png"))
    private val iconNoSelected = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/no_activated.png"))

    private val iconYes = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/yes.png"))
    private val iconYesSelected = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/yes_activated.png"))

    init {

        val scrollable = GuiComponent(2, 2 + 16, componentMenuBackground.widthi - 4, 5 * 24 + 5 * 2)
        scrollable.clipToBounds = true

        scrollable.BUS.hook(GuiComponentEvents.MouseWheelEvent::class.java) {
            when (it.direction) {
                GuiComponentEvents.MouseWheelDirection.UP ->
                    scrollable.contentsOffset = vec(0, max(-16.0, scrollable.contentsOffset.y - 12))
                GuiComponentEvents.MouseWheelDirection.DOWN ->
                    scrollable.contentsOffset = vec(0, min(0.0, scrollable.contentsOffset.y + 12))
            }
        }

        componentMenuBackground.add(scrollable)

        val notifText = TextLayer(2, 2, componentMenuBackground.widthi - 4, 16)
        notifText.text = "Duel Requests:"
        notifText.align = Align2d.CENTER
        notifText.maxLines = 2
        notifText.lineSpacing = 1
        componentMenuBackground.add(notifText)


        val players = Minecraft().world.playerEntities
                .map { Utils.getPlayerData(it.uniqueID) }
                .filterNotNull().map { it to it.challengedMode }
                .filter { it.first.challenged == Minecraft().player.uniqueID }
        for ((i, pair) in players.withIndex()) {

            val request = ComponentRect(0, i * 24 + i * 2, scrollable.widthi, 20)
            request.color = Color(255, 244, 178)

            val friendIcon = ComponentSprite(iconFriend, 2, 2, 16, 16)
            request.add(friendIcon)

            val nameText = TextLayer(20, 0, request.widthi - 20 - 32 - 8, 20)
            nameText.text = pair.first.name + " (${pair.second!!.name})"
            nameText.truncate = true
            nameText.align = Align2d.LEFT_CENTER
            nameText.maxLines = 2
            nameText.lineSpacing = 1
            request.add(nameText)

            val accept = ComponentSprite(iconYes, request.widthi - 16 - 16 - 4, 2, 16, 16)
            accept.BUS.hook(GuiComponentEvents.MouseMoveInEvent::class.java) {
                accept.sprite = iconYesSelected
            }
            accept.BUS.hook(GuiComponentEvents.MouseMoveOutEvent::class.java) {
                accept.sprite = iconYes
            }
            accept.BUS.hook(GuiComponentEvents.MouseClickEvent::class.java) {
                Minecraft().player.sendChatMessage("/duel ${pair.first.name} accept")
            }
            request.add(accept)

            val deny = ComponentSprite(iconNo, request.widthi - 16 - 2, 2, 16, 16)
            deny.BUS.hook(GuiComponentEvents.MouseMoveInEvent::class.java) {
                deny.sprite = iconNoSelected
            }
            deny.BUS.hook(GuiComponentEvents.MouseMoveOutEvent::class.java) {
                deny.sprite = iconNo
            }
            deny.BUS.hook(GuiComponentEvents.MouseClickEvent::class.java) {
                Minecraft().player.sendChatMessage("/duel ${pair.first.name} deny")
            }
            request.add(deny)

            scrollable.add(request)

        }


        val addFriend = ComponentText(2, componentMenuBackground.heighti - 16 - 11)
        addFriend.text = "Send Duel Request:"
        componentMenuBackground.add(addFriend)

        val fieldRect = ComponentRect(2, componentMenuBackground.heighti - 18, componentMenuBackground.widthi - 4, 13)
        fieldRect.color = Color(255, 255, 255)
        componentMenuBackground.add(fieldRect)

        val field = ComponentTextField(Minecraft.getMinecraft().fontRenderer, 2, heighti - 16, componentMenuBackground.widthi - 10, 16)
        field.isFocused = true
        field.translateZ = 100.0
        field.canLoseFocus = false
        field.useShadow = false

        field.enabledColor = Color.BLACK
        field.cursorColor = Color.BLACK
        field.disabledColor = Color.GRAY
        field.selectionColor = Color.GRAY
        field.BUS.hook(ComponentTextField.TextSentEvent::class.java) {
            val name = it.content.trim()
            if (name.isEmpty()) {
                return@hook
            }
            Minecraft().player.sendChatMessage("/duel $name half")

            field.writeText("")
        }

        componentMenuBackground.add(field)
    }

    override fun wipeIn(): Float {
        if (parentSpace != null) {
            val origin = gui.categoryButtons[0]?.convertPointTo(Vec2d.ZERO, parentSpace) ?: Vec2d.ZERO
            val newPos = Vec2d(origin.x - 5 - width, origin.y + ((gui.categoryCount * 32) / 2.0) - (height / 2.0))
            pos = newPos
        }
        return super.wipeIn()
    }
}