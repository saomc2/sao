package com.teamwizardry.sao.client.gui

import com.teamwizardry.librarianlib.features.animator.Easing
import com.teamwizardry.librarianlib.features.animator.animations.BasicAnimation
import com.teamwizardry.librarianlib.features.animator.animations.ScheduledEventAnimation
import com.teamwizardry.librarianlib.features.gui.component.GuiComponent
import com.teamwizardry.librarianlib.features.gui.component.GuiLayer
import com.teamwizardry.librarianlib.features.gui.components.ComponentRect
import com.teamwizardry.librarianlib.features.gui.components.ComponentSprite
import com.teamwizardry.librarianlib.features.gui.components.ComponentVoid
import com.teamwizardry.librarianlib.features.sprite.Sprite
import com.teamwizardry.sao.SAO
import net.minecraft.util.ResourceLocation
import java.awt.Color
import kotlin.math.abs

abstract class ComponentWindow(val widthX: Int, val heightX: Int, val arrowDir: EnumWindowSide = EnumWindowSide.LEFT) : GuiComponent(0, 0, widthX + 8, heightX), ISlidableComponent {

    protected val triangleRight = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/triangle_right.png"))
    protected val triangleLeft = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/triangle_left.png"))

    protected val maskWipe: ComponentVoid = ComponentVoid(0, 0, 0, (widthX + 8) / 10 + heightX + (widthX + 8) / 10)
    protected val reverseMaskWipe = GuiComponent(widthX + 8, -1000, widthX + 8, 2000)
    protected var componentMenuBackground: ComponentRect = ComponentRect(0, 0, 0, 0)
    protected var componentMenuBorderTop: ComponentRect = ComponentRect(0, 0, 0, 0)
    protected var componentMenuBorderBottom: ComponentRect = ComponentRect(0, 0, 0, 0)
    protected var componentArrow: ComponentSprite = ComponentSprite(triangleRight, 0, 0, 0, 0)

    protected val slides = ArrayList<ComponentSlide>()

    init {
        isVisible = false
        maskWipe.clipToBounds = true
        maskWipe.isOpaqueToMouse = false
        maskWipe.propagateMouse = false
        maskWipe.addTag("mask")
        add(maskWipe)

        reverseMaskWipe.clipToBounds = true
        reverseMaskWipe.isOpaqueToMouse = false
        reverseMaskWipe.propagateMouse = false
        reverseMaskWipe.addTag("mask")
        add(reverseMaskWipe)

        when (arrowDir) {
            EnumWindowSide.LEFT -> {
                componentArrow = ComponentSprite(triangleLeft, 0, (heightX / 2) + (24 / 2) - (8 / 2), 8, 8)
                maskWipe.add(componentArrow)

                val borderY = (widthX + 8) / 10
                componentMenuBorderTop = ComponentRect(8, 0, widthX - 8, borderY)
                componentMenuBackground = ComponentRect(8, borderY, widthX - 8, heightX)
                componentMenuBackground.color = (Color(0xDDDDDD))
                componentMenuBorderBottom = ComponentRect(8, borderY + heightX, widthX - 8, borderY)
                maskWipe.add(componentMenuBorderTop, componentMenuBackground, componentMenuBorderBottom)
            }
            EnumWindowSide.RIGHT -> {
                val borderY = (widthX + 8) / 10
                componentMenuBorderTop = ComponentRect(-widthX - 8, 1000, widthX - 8, borderY.toInt())
                componentMenuBackground = ComponentRect(-widthX - 8, 1000 + borderY.toInt(), widthX - 8, heightX)
                componentMenuBackground.color = (Color(0xDDDDDD))
                componentMenuBorderBottom = ComponentRect(-widthX - 8, 1000 + borderY.toInt() + heightX, widthX - 8, borderY.toInt())
                componentArrow = ComponentSprite(triangleRight, componentMenuBackground.widthi, (componentMenuBackground.heighti / 2) - (8 / 2) - (24 / 2), 8, 8)
                componentMenuBackground.add(componentArrow)
                reverseMaskWipe.add(componentMenuBorderTop, componentMenuBackground, componentMenuBorderBottom)
            }
        }
    }

    fun addSlide(vararg slides: ComponentSlide) {
        for (slide in slides) {
            componentMenuBackground.add(slide)
            this.slides.add(slide)
        }
    }

    override fun wipeIn(): Float {
        isVisible = true

        if (arrowDir == EnumWindowSide.LEFT) {
            val animWidthWipe: BasicAnimation<ComponentVoid> = BasicAnimation(maskWipe, "size.x")
            animWidthWipe.duration = 20f
            animWidthWipe.easing = Easing.easeOutQuint
            animWidthWipe.to = (width + 8)
            add(animWidthWipe)
        } else {
            val animProfileWipeSustainer1: BasicAnimation<ComponentRect> = BasicAnimation(componentMenuBorderTop, "pos.x")
            animProfileWipeSustainer1.duration = 20f
            animProfileWipeSustainer1.easing = Easing.easeOutQuint
            animProfileWipeSustainer1.to = 0
            add(animProfileWipeSustainer1)

            val animProfileWipeSustainer2: BasicAnimation<ComponentRect> = BasicAnimation(componentMenuBackground, "pos.x")
            animProfileWipeSustainer2.duration = 20f
            animProfileWipeSustainer2.easing = Easing.easeOutQuint
            animProfileWipeSustainer2.to = 0
            add(animProfileWipeSustainer2)

            val animProfileWipeSustainer3: BasicAnimation<ComponentRect> = BasicAnimation(componentMenuBorderBottom, "pos.x")
            animProfileWipeSustainer3.duration = 20f
            animProfileWipeSustainer3.easing = Easing.easeOutQuint
            animProfileWipeSustainer3.to = 0
            add(animProfileWipeSustainer3)

            val animProfileWipeSustainer4: BasicAnimation<ComponentSprite> = BasicAnimation(componentArrow, "pos.x")
            animProfileWipeSustainer4.duration = 10f
            animProfileWipeSustainer4.easing = Easing.easeOutQuint
            animProfileWipeSustainer4.to = width - 16
            //   add(animProfileWipeSustainer4)

            val animProfileWipe: BasicAnimation<GuiComponent> = BasicAnimation(reverseMaskWipe, "pos.x")
            animProfileWipe.duration = 20f
            animProfileWipe.easing = Easing.easeOutQuint
            animProfileWipe.to = 0
            add(animProfileWipe)
        }

        return 20f
    }

    override fun wipeOut(): Float {
        val animEnd = ScheduledEventAnimation(10f) {
            isVisible = false
        }
        add(animEnd)

        if (arrowDir == EnumWindowSide.LEFT) {
            val animWidthWipe: BasicAnimation<ComponentVoid> = BasicAnimation(maskWipe, "size.x")
            animWidthWipe.duration = 10f
            animWidthWipe.easing = Easing.easeOutQuint
            animWidthWipe.to = 0
            add(animWidthWipe)
        } else {

            val animProfileWipeSustainer1: BasicAnimation<ComponentRect> = BasicAnimation(componentMenuBorderTop, "pos.x")
            animProfileWipeSustainer1.duration = 10f
            animProfileWipeSustainer1.easing = Easing.easeOutQuint
            animProfileWipeSustainer1.to = -width
            add(animProfileWipeSustainer1)

            val animProfileWipeSustainer2: BasicAnimation<ComponentRect> = BasicAnimation(componentMenuBackground, "pos.x")
            animProfileWipeSustainer2.duration = 10f
            animProfileWipeSustainer2.easing = Easing.easeOutQuint
            animProfileWipeSustainer2.to = -width
            add(animProfileWipeSustainer2)

            val animProfileWipeSustainer3: BasicAnimation<ComponentRect> = BasicAnimation(componentMenuBorderBottom, "pos.x")
            animProfileWipeSustainer3.duration = 10f
            animProfileWipeSustainer3.easing = Easing.easeOutQuint
            animProfileWipeSustainer3.to = -width
            add(animProfileWipeSustainer3)

            val animProfileWipeSustainer4: BasicAnimation<ComponentSprite> = BasicAnimation(componentArrow, "pos.x")
            animProfileWipeSustainer4.duration = 10f
            animProfileWipeSustainer4.easing = Easing.easeOutQuint
            animProfileWipeSustainer4.to = 0
            //  add(animProfileWipeSustainer4)

            val animProfileWipe: BasicAnimation<GuiComponent> = BasicAnimation(reverseMaskWipe, "pos.x")
            animProfileWipe.duration = 10f
            animProfileWipe.easing = Easing.easeOutQuint
            animProfileWipe.to = width
            add(animProfileWipe)
        }
        return 10f
    }

    class ComponentSlide(val window1: ComponentWindow, val side: EnumWindowSide) : GuiComponent(0, 0, window1.componentMenuBackground.widthi, window1.componentMenuBackground.heighti), ISlidableComponent {

        protected val mask = GuiComponent(0, -1000, 0, 2000)
        protected val base = GuiComponent(2, abs(mask.y).toInt() + 2, widthi - 4, heighti - 4)

        init {
            mask.clipToBounds = true
            mask.isOpaqueToMouse = false
            mask.propagateMouse = false
            mask.addTag("mask")
            add(mask)

            if (side == EnumWindowSide.LEFT) {
                mask.pos = mask.pos.setX(width)
                base.pos = base.pos.setX(-width)
            } else {
                mask.pos = mask.pos.setX(0.0)
                base.pos = base.pos.setX(2.0)
            }

            mask.add(base)
        }

        fun addToBase(vararg comps: GuiLayer) {
            for (comp in comps) {
                base.add(comp)
            }
        }

        override fun wipeIn(): Float {
            var time = 0f
            for (slide in window1.slides) {
                if (slide != this)
                    time = slide.wipeOut()
            }
            val animOutSlides = ScheduledEventAnimation(time) {
                isVisible = true

                val animMaskPos: BasicAnimation<GuiComponent> = BasicAnimation(mask, "pos.x")
                animMaskPos.duration = 20f
                animMaskPos.easing = Easing.easeOutQuint
                animMaskPos.to = 0.0
                add(animMaskPos)

                val animMaskSize: BasicAnimation<GuiComponent> = BasicAnimation(mask, "size.x")
                animMaskSize.duration = 20f
                animMaskSize.easing = Easing.easeOutQuint
                animMaskSize.to = width
                add(animMaskSize)

                val animBasePos: BasicAnimation<GuiComponent> = BasicAnimation(base, "pos.x")
                animBasePos.duration = 20f
                animBasePos.easing = Easing.easeOutQuint
                animBasePos.to = 2.0
                add(animBasePos)
            }

            add(animOutSlides)

            return 10f
        }

        override fun wipeOut(): Float {

            val animMaskPos: BasicAnimation<GuiComponent> = BasicAnimation(mask, "pos.x")
            animMaskPos.duration = 10f
            animMaskPos.easing = Easing.easeOutQuint
            animMaskPos.to = if (side == EnumWindowSide.LEFT) width else 0.0
            animMaskPos.completion = Runnable {
                isVisible = false
            }
            add(animMaskPos)

            val animMaskSize: BasicAnimation<GuiComponent> = BasicAnimation(mask, "size.x")
            animMaskSize.duration = 10f
            animMaskSize.easing = Easing.easeOutQuint
            animMaskSize.to = 0.0
            add(animMaskSize)

            val animBasePos: BasicAnimation<GuiComponent> = BasicAnimation(base, "pos.x")
            animBasePos.duration = 10f
            animBasePos.easing = Easing.easeOutQuint
            animBasePos.to = if (side == EnumWindowSide.LEFT) -width else 2.0
            add(animBasePos)

            return 10f
        }
    }

    enum class EnumWindowSide {

        LEFT, RIGHT
    }
}