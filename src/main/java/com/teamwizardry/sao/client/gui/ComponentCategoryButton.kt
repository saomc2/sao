package com.teamwizardry.sao.client.gui

import com.teamwizardry.librarianlib.features.animator.animations.ScheduledEventAnimation
import com.teamwizardry.librarianlib.features.gui.component.GuiComponent
import com.teamwizardry.librarianlib.features.gui.component.GuiComponentEvents
import com.teamwizardry.librarianlib.features.gui.components.ComponentSprite
import com.teamwizardry.librarianlib.features.math.Vec2d
import com.teamwizardry.librarianlib.features.sprite.Sprite

class ComponentCategoryButton(gui: GuiSAOUI, index: Int, size: Int, nbOfCategories: Int, mainComp: GuiComponent, icon_off: Sprite, icon_on: Sprite, val childComponent: GuiComponent?) :
        GuiComponent(
                (mainComp.widthi / 2) - (size / 2),
                (mainComp.heighti / 2) - ((size * nbOfCategories) / 2),
                size,
                size) {

    var isSelected = false

    init {
        gui.categoryButtons[index] = this

        val componentSprite = ComponentSprite(icon_off, 0, 0, 32, 32)
        add(componentSprite)

        childComponent?.pos = Vec2d(size.toDouble(), 0.0)
        childComponent?.isVisible = false
        if (childComponent != null) add(childComponent)

        BUS.hook(GuiComponentEvents.MouseEnterEvent::class.java) {
            componentSprite.sprite = icon_on
        }
        BUS.hook(GuiComponentEvents.MouseLeaveEvent::class.java) {
            componentSprite.sprite = icon_off
        }

        BUS.hook(GuiComponentEvents.MouseClickEvent::class.java) {
            if (!isSelected) {
                isSelected = true

                var endTime = 0f

                for (category in gui.categoryButtons) {
                    if (category?.isSelected == true && category != this) {
                        category.isSelected = false

                        if (category.childComponent is ISlidableComponent) {
                            endTime = category.childComponent.wipeOut()
                        }
                    }
                }

                val animEnd = ScheduledEventAnimation(endTime) {
                    if (childComponent is ISlidableComponent) {
                        childComponent.wipeIn()
                    }
                }
                add(animEnd)

            } else {
                isSelected = false

                if (childComponent is ISlidableComponent) {
                    childComponent.wipeOut()
                    gui.moveTo()
                }
            }
        }
    }
}