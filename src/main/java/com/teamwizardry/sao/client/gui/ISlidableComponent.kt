package com.teamwizardry.sao.client.gui

interface ISlidableComponent {

    fun wipeIn() : Float
    fun wipeOut() : Float

    //fun reset()
}