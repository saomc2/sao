package com.teamwizardry.sao.client.gui

import com.teamwizardry.librarianlib.features.animator.Easing
import com.teamwizardry.librarianlib.features.animator.animations.BasicAnimation
import com.teamwizardry.librarianlib.features.animator.animations.ScheduledEventAnimation
import com.teamwizardry.librarianlib.features.gui.component.GuiComponent
import com.teamwizardry.librarianlib.features.gui.component.GuiLayerEvents
import com.teamwizardry.librarianlib.features.gui.components.ComponentSprite
import com.teamwizardry.librarianlib.features.gui.components.ComponentVoid
import com.teamwizardry.librarianlib.features.math.Vec2d
import com.teamwizardry.librarianlib.features.sprite.Sprite
import com.teamwizardry.sao.SAO
import net.minecraft.client.renderer.GlStateManager
import net.minecraft.util.ResourceLocation
import java.util.*

class ComponentPlayerProfile(val gui: GuiSAOUI, val uuid: UUID) : GuiComponent(0, 0, background.width, background.height), ISlidableComponent {

    companion object {
        protected val background = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/profilebg.png"))
    }

    private val reverseMaskWipe = ComponentVoid(widthi, -1000, widthi, 2000)
    private val componentBackground = ComponentSprite(background, -widthi, 1000, background.width, background.height)

    init {
        isVisible = false
        reverseMaskWipe.clipToBounds = true
        reverseMaskWipe.isOpaqueToMouse = false
        reverseMaskWipe.propagateMouse = false
        reverseMaskWipe.addTag("mask")
        add(reverseMaskWipe)

        reverseMaskWipe.add(componentBackground)

        val player = GuiSAOUI.getPlayerData(uuid)

        val res = 50
        val width = 100
        val height = 124

        val componentEntity = ComponentVoid(componentBackground.widthi / 2 - (width / 2), 35, width, height)
        componentEntity.clipToBounds = true
        componentEntity.BUS.hook(GuiLayerEvents.PostDrawEvent::class.java) {
            if (player == null) return@hook
            GlStateManager.pushMatrix()
        //   GuiInventory.drawEntityOnScreen(width / 2, (height / 2) + res, res, (width / 2) - mousePos.xf, (height / 2) - (res / 2) - mousePos.yf, player)
            GlStateManager.enableBlend()
            GlStateManager.popMatrix()
        }
        componentBackground.add(componentEntity)
    }

    override fun wipeIn(): Float {
        isVisible = true

        if (parentSpace != null) {
            val origin = gui.categoryButtons[0]?.convertPointTo(Vec2d.ZERO, parentSpace) ?: Vec2d.ZERO
            val newPos = Vec2d(origin.x - 5 - width, origin.y + ((gui.categoryCount * 32) / 2.0) - (height / 2.0))
            pos = newPos
        }

        val animProfileWipeSustainer: BasicAnimation<ComponentSprite> = BasicAnimation(componentBackground, "pos.x")
        animProfileWipeSustainer.duration = 20f
        animProfileWipeSustainer.easing = Easing.easeOutQuint
        animProfileWipeSustainer.to = 0
        add(animProfileWipeSustainer)

        val animProfileWipe: BasicAnimation<ComponentVoid> = BasicAnimation(reverseMaskWipe, "pos.x")
        animProfileWipe.duration = 20f
        animProfileWipe.easing = Easing.easeOutQuint
        animProfileWipe.to = 0
        add(animProfileWipe)

        return 20f
    }

    override fun wipeOut(): Float {
        val animEnd = ScheduledEventAnimation(10f) {
            isVisible = false
        }
        add(animEnd)

        val animProfileWipeSustainer: BasicAnimation<ComponentSprite> = BasicAnimation(componentBackground, "pos.x")
        animProfileWipeSustainer.duration = 10f
        animProfileWipeSustainer.easing = Easing.easeOutQuint
        animProfileWipeSustainer.to = -width
        add(animProfileWipeSustainer)

        val animProfileWipe: BasicAnimation<ComponentVoid> = BasicAnimation(reverseMaskWipe, "pos.x")
        animProfileWipe.duration = 10f
        animProfileWipe.easing = Easing.easeOutQuint
        animProfileWipe.to = width
        add(animProfileWipe)

        return 10f
    }
}