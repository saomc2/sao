package com.teamwizardry.sao.client.gui

import com.teamwizardry.librarianlib.features.animator.Easing
import com.teamwizardry.librarianlib.features.animator.animations.BasicAnimation
import com.teamwizardry.librarianlib.features.animator.animations.ScheduledEventAnimation
import com.teamwizardry.librarianlib.features.gui.component.GuiComponent
import com.teamwizardry.librarianlib.features.gui.component.GuiComponentEvents
import com.teamwizardry.librarianlib.features.gui.components.ComponentRect
import com.teamwizardry.librarianlib.features.gui.components.ComponentSprite
import com.teamwizardry.librarianlib.features.gui.components.ComponentText
import com.teamwizardry.librarianlib.features.kotlin.Minecraft
import com.teamwizardry.librarianlib.features.kotlin.plus
import com.teamwizardry.librarianlib.features.math.Vec2d
import com.teamwizardry.librarianlib.features.sprite.Sprite
import net.minecraft.util.text.TextFormatting
import java.awt.Color
import java.util.function.Consumer

class ComponentMenuSlot(
        val gui: GuiSAOUI,
        private val swappableHolder: ISwappableHolder,
        private val index: Int,
        private val spriteOff: Sprite,
        private val spriteOn: Sprite,
        private val title: String,
        private val componentChild: GuiComponent?,
        enableChildPositionHandling: Boolean = true,
        private val onClicked: Consumer<ComponentMenuSlot>? = null,
        private val onUnclicked: Consumer<ComponentMenuSlot>? = null
) : GuiComponent(0, (index * 24) + (index * 2), 100, 24) {

    init {
        setData(Int::class.java, "index", index)

        val rect = ComponentRect(0, 0, widthi, heighti)
        add(rect)

        val iconComp = ComponentSprite(spriteOff, 5, (heighti / 2) - (16 / 2), 16, 16)
        val textComp = ComponentText(5 + iconComp.widthi + 5, (heighti / 2) - (Minecraft().fontRenderer.FONT_HEIGHT / 2))
        textComp.text = (title)
        add(iconComp, textComp)

        if (enableChildPositionHandling)
            componentChild?.pos = Vec2d(widthi + 5.0, if (componentChild == null) 0.0 else -componentChild.heighti / 2.0)
        componentChild?.isVisible = false
        if (componentChild != null) {
            add(componentChild)
        }

        BUS.hook(GuiComponentEvents.MouseEnterEvent::class.java) {
            iconComp.sprite = spriteOn
            textComp.text = (TextFormatting.ITALIC + " " + title)
            rect.color = (Color(0xE8A603))
        }

        BUS.hook(GuiComponentEvents.MouseLeaveEvent::class.java) {
            iconComp.sprite = spriteOff
            textComp.text = (title)
            rect.color = (Color.WHITE)
        }

        BUS.hook(GuiComponentEvents.MouseClickEvent::class.java) {
            onClick()
        }
    }

    fun onClick() {
        val endTime = swappableHolder.getCurrentOpenComponent()?.wipeOut() ?: 0f

        val animWipeOutsFinish = ScheduledEventAnimation(endTime) {

            val scrollToCenter: BasicAnimation<GuiComponent> = BasicAnimation(swappableHolder.getMenuHolder(), "pos.y")
            scrollToCenter.duration = 10f
            scrollToCenter.easing = Easing.easeOutBack
            scrollToCenter.to = (32 / 2) - (heighti / 2) - (index * 24) - (index * 2)
            add(scrollToCenter)

            if (componentChild == null) {
                onClicked?.accept(this)
            }

            if (swappableHolder.getCurrentOpenComponent() != componentChild) {

                if (componentChild == null) {

                    swappableHolder.setCurrentOpenComponent(null)

                } else if (componentChild is ISlidableComponent) {
                    componentChild.isVisible = true

                    if (swappableHolder is ISlidableComponent)
                        swappableHolder.wipeIn()

                    onClicked?.accept(this)
                    val schedEndTime = ScheduledEventAnimation(10f) {
                        componentChild.wipeIn()
                        swappableHolder.setCurrentOpenComponent(componentChild)
                    }
                    add(schedEndTime)
                }


            } else {
                if (componentChild is ISlidableComponent)
                    componentChild.wipeOut()

                swappableHolder.setCurrentOpenComponent(null)
                onUnclicked?.accept(this)
            }
        }
        add(animWipeOutsFinish)
    }
}