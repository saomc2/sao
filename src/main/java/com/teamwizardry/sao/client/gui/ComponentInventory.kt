package com.teamwizardry.sao.client.gui

import com.teamwizardry.librarianlib.features.gui.component.GuiComponent
import com.teamwizardry.librarianlib.features.gui.component.GuiComponentEvents
import com.teamwizardry.librarianlib.features.gui.components.ComponentSprite
import com.teamwizardry.librarianlib.features.gui.components.ComponentStack
import com.teamwizardry.librarianlib.features.gui.components.ComponentText
import com.teamwizardry.librarianlib.features.kotlin.Minecraft
import com.teamwizardry.librarianlib.features.kotlin.isNotEmpty
import com.teamwizardry.librarianlib.features.kotlin.plus
import com.teamwizardry.librarianlib.features.network.PacketHandler
import com.teamwizardry.librarianlib.features.sprite.Sprite
import com.teamwizardry.sao.SAO
import com.teamwizardry.sao.api.capability.player.ISAOPlayer
import com.teamwizardry.sao.api.capability.player.SAOPlayerCap
import com.teamwizardry.sao.common.network.PacketUpdateCapsFromClient
import net.minecraft.item.ItemStack
import net.minecraft.util.ResourceLocation
import net.minecraft.util.text.TextFormatting
import java.awt.Color

class ComponentInventory(val gui: GuiSAOUI?) : ComponentWindow(316, 150) {

    companion object {
        protected val blank = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/itemcircle.png"))
        protected val blankSelected = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/itemcircle_activated.png"))
    }

    val inventory = HashSet<ComponentItemSlot>()
    val favorites = HashSet<ComponentItemSlot>()

    override fun wipeIn(): Float {
        gui?.moveTo(widthi / 2)
        return super.wipeIn()
    }

    override fun wipeOut(): Float {
        gui?.moveTo(0)
        return super.wipeOut()
    }

    init {
        val titleText = "INVENTORY"
        val componentTitle = ComponentText(componentMenuBorderTop.widthi / 2, componentMenuBorderTop.heighti / 2, ComponentText.TextAlignH.CENTER, ComponentText.TextAlignV.MIDDLE)
        componentTitle.text = TextFormatting.BOLD + titleText
        componentMenuBorderTop.add(componentTitle)

        var lastSlot = 0

        var row = -1
        for (i in 0..26) {
            if (i % 9 == 0) {
                row++
            }

            val x = i % 9 * 32 + (i % 9 * 2) + 2
            val y = row * 32 + (row * 2) + 2

            var stack = ItemStack.EMPTY
            for (j in lastSlot..26) {
                stack = Minecraft().player.inventory.getStackInSlot(j + 9)
                if (stack.isEmpty) continue

                lastSlot = j + 1
                break
            }

            val slot = ComponentItemSlot(stack, i, false, favorites, inventory, x, y)
            componentMenuBackground.add(slot)
        }

        for (i in 0..8) {
            val x = i * 32 + (i * 2) + 2

            val stack = SAOPlayerCap.get(Minecraft().player)?.favoriteItems?.get(i) ?: ItemStack.EMPTY
            val slot = ComponentItemSlot(stack, i, true, favorites, inventory, x, componentMenuBackground.heighti - 32 - 2)
            componentMenuBackground.add(slot)
        }
    }

    class ComponentItemSlot(val slotStack: ItemStack, private val index: Int, val isFavoriteSlot: Boolean, val favorites: HashSet<ComponentItemSlot>, val inventory: HashSet<ComponentItemSlot>, posX: Int, posY: Int) : GuiComponent(posX, posY, 32, 32), ISlidableComponent {

        private val componentSlot = ComponentSprite(blank, 0, 0, 32, 32)
        private var bubbleMenu: ComponentBubbleMenu
        private val componentStack = ComponentStack((32 / 2) - (16 / 2), (32 / 2) - (16 / 2))

        init {
            // --username=saadodi44@gmail.com --password=tangleberry44
            if (isFavoriteSlot) {
                favorites.add(this)
            } else inventory.add(this)

            add(componentSlot)

            componentStack.stack = slotStack
            componentStack.zIndex = 3.0
            componentSlot.add(componentStack)

            bubbleMenu = ComponentBubbleMenu(this)
            componentSlot.add(bubbleMenu)

            componentSlot.BUS.hook(GuiComponentEvents.MouseEnterEvent::class.java) {
                componentSlot.sprite = blankSelected
            }
            componentSlot.BUS.hook(GuiComponentEvents.MouseLeaveEvent::class.java) {
                componentSlot.sprite = blank
            }
            componentSlot.BUS.hook(GuiComponentEvents.MouseClickEvent::class.java) {
                val cap = SAOPlayerCap.get(Minecraft().player) ?: return@hook

                if (hasTag("isSelected")) {

                    if (isFavoriteSlot) {
                        setStack(ItemStack.EMPTY, cap, true)
                        wipeOut()
                    } else {

                        wipeIn()
                    }

                    removeTag("isSelected")

                } else {

                    if (!isFavoriteSlot) {
                        for (otherSlots in inventory) {
                            if (otherSlots == this) continue

                            otherSlots.wipeOut()
                            otherSlots.removeTag("isSelected")
                        }

                        componentSlot.sprite = blankSelected
                        componentSlot.color = Color.ORANGE

                        addTag("isSelected")

                    } else {
                        for (otherSlots in favorites) {
                            if (otherSlots == this) continue

                            otherSlots.wipeOut()
                            otherSlots.removeTag("isSelected")
                        }

                        for (invSlot in inventory) {

                            if (invSlot.hasTag("isSelected")) {

                                for (otherSlot in favorites) {
                                    if (otherSlot == this) continue

                                    val stack2 = otherSlot.getStack()
                                    if (ItemStack.areItemStacksEqual(invSlot.getStack(), stack2)) {
                                        otherSlot.setStack(ItemStack.EMPTY, cap, true)
                                    }
                                }

                                invSlot.removeTag("isSelected")
                                setStack(invSlot.getStack(), cap, true)

                                wipeOut()
                                invSlot.wipeOut()

                                return@hook
                            }
                        }

                        if (componentStack.stack.isNotEmpty) {
                            componentSlot.sprite = blankSelected
                            componentSlot.color = Color.ORANGE
                            addTag("isSelected")
                        }
                    }
                }
            }
        }

        fun getStack() = componentStack.stack

        fun setStack(newStack: ItemStack = ItemStack.EMPTY, cap: ISAOPlayer? = null, setFavorite: Boolean = false) {
            componentStack.stack = newStack

            cap?.let {
                if (setFavorite)
                    cap.favoriteItems[index] = newStack
                PacketHandler.NETWORK.sendToServer(PacketUpdateCapsFromClient(cap.serializeNBT(), Minecraft().player.uniqueID))
            }
        }

        override fun wipeIn(): Float {
            zIndex = 1.0
            componentSlot.translateZ = 50.0
            componentSlot.sprite = blankSelected
            componentSlot.color = Color.ORANGE

            return if (getStack().isNotEmpty) bubbleMenu.wipeIn() else 0f
        }

        override fun wipeOut(): Float {
            zIndex = 0.0
            componentSlot.translateZ = 0.0
            componentSlot.sprite = blank
            componentSlot.color = Color.WHITE

            return bubbleMenu.wipeOut()
        }
    }
}
