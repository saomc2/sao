package com.teamwizardry.sao.client.gui

import com.teamwizardry.librarianlib.features.animator.Easing
import com.teamwizardry.librarianlib.features.animator.animations.BasicAnimation
import com.teamwizardry.librarianlib.features.gui.component.GuiComponent
import com.teamwizardry.librarianlib.features.gui.component.GuiComponentEvents
import com.teamwizardry.librarianlib.features.gui.components.ComponentRect
import com.teamwizardry.librarianlib.features.gui.components.ComponentSprite
import com.teamwizardry.librarianlib.features.gui.components.ComponentVoid
import com.teamwizardry.librarianlib.features.math.Vec2d
import com.teamwizardry.librarianlib.features.sprite.Sprite
import com.teamwizardry.sao.SAO
import com.teamwizardry.sao.api.util.RandUtil
import net.minecraft.util.ResourceLocation
import java.awt.Color

abstract class ComponentMenu(val isFixedMenu: Boolean = false) : GuiComponent(0, 0, 0, 0), ISlidableComponent, ISwappableHolder {

    companion object {
        private val slider = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/selection_slider.png"))
    }

    private var currentMenu: ISlidableComponent? = null
    private val rect = ComponentRect(-1, -1, 0, 0)
    private var menuSlotsBox: GuiComponent = ComponentVoid(1000, 1000, 0, 0)
    private var menuBox: GuiComponent = ComponentVoid(10, (32 / 2) - (24 / 2) - (1 * 24) - (1 * 2), 0, 0)

    private val maskWipe = ComponentRect(-1000, -1000, 1000, 2000)

    private val slots = ArrayList<ComponentMenuSlot>()

    private val componentSlider = ComponentSprite(slider, 0, (-slider.height / 2) + (32 / 2), slider.width, slider.height)
    private var addSlider = false

    init {
        slots.clear()
        isVisible = false
        maskWipe.clipToBounds = true
        maskWipe.isOpaqueToMouse = false
        maskWipe.propagateMouse = false
        maskWipe.addTag("mask")
        maskWipe.color = Color(RandUtil.nextFloat(), RandUtil.nextFloat(), RandUtil.nextFloat(), 0f)
        add(maskWipe)

        menuBox = ComponentVoid(1000, 1000, 0, 0)
        maskWipe.add(menuBox)
        
        if (isFixedMenu) {
            rect.color = (Color(0f, 0f, 0f, 0.75f))
            menuSlotsBox.add(rect)

        } else menuSlotsBox = ComponentVoid(10, (32 / 2) - (24 / 2) - (1 * 24), 100, menuBox.heighti)

        menuSlotsBox = ComponentVoid(10, (32 / 2) - (24 / 2) - (1 * 24) - (1 * 2), 0, 0)
        menuBox.add(menuSlotsBox)

        menuSlotsBox.BUS.hook(GuiComponentEvents.MouseWheelEvent::class.java) {

        }
    }

    fun addMenuSlots(vararg menuSlots: ComponentMenuSlot) {
        slots.addAll(menuSlots)

        for (slot in slots) {
            menuSlotsBox.add(slot)
            menuSlotsBox.size = Vec2d(100.0, ((slots.size) * 24) + ((slots.size) * 2.0))
            menuSlotsBox.pos = Vec2d(menuSlotsBox.pos.x, (32.0 / 2.0) - (24.0 / 2.0) - ((slots.size / 2) * 24) - ((slots.size / 2) * 2.0))
            rect.size = Vec2d(menuSlotsBox.width + 2.0, menuSlotsBox.height)
        }

        if (!addSlider) {
            addSlider = true
            menuBox.add(componentSlider)
        }
    }

    override fun getCurrentOpenComponent(): ISlidableComponent? {
        return currentMenu
    }

    override fun setCurrentOpenComponent(component: ISlidableComponent?) {
        currentMenu = component
    }

    override fun getTotalWidth(): Int {
        return menuBox.getContentsBounds(includeOwnBounds = {
            it is GuiComponent && !it.hasTag("mask") && it.isVisible
        }, includeChildren = {
            it is GuiComponent && it.isVisible
        })?.widthi ?: 0
    }

    override fun getMenuHolder(): GuiComponent {
        return menuSlotsBox
    }

    override fun wipeIn(): Float {
        isVisible = true

        maskWipe.clipToBounds = true
        val animWidthWipe: BasicAnimation<GuiComponent> = BasicAnimation(maskWipe, "size.x")
        animWidthWipe.duration = 20f
        animWidthWipe.easing = Easing.easeOutQuint
        animWidthWipe.to = 1000 + getTotalWidth()
        animWidthWipe.completion = Runnable {
            maskWipe.clipToBounds = false
        }
        add(animWidthWipe)

        return 20f
    }

    override fun wipeOut(): Float {
        for (child in allChildren) {
            if (child is ISlidableComponent) {
                child.wipeOut()
            }
        }

        maskWipe.clipToBounds = true
        val animWidthWipe: BasicAnimation<GuiComponent> = BasicAnimation(maskWipe, "size.x")
        animWidthWipe.duration = 10f
        animWidthWipe.easing = Easing.easeOutQuint
        animWidthWipe.to = 1000
        animWidthWipe.completion = Runnable {
            isVisible = false
            currentMenu = null
            maskWipe.clipToBounds = false

            menuSlotsBox.pos = Vec2d(15.0, (32.0 / 2.0) - (24.0 / 2.0) - (1.0 * 24.0))
        }
        maskWipe.add(animWidthWipe)

        return 10f
    }
}