package com.teamwizardry.sao.client.gui

import com.teamwizardry.librarianlib.features.kotlin.Minecraft
import com.teamwizardry.librarianlib.features.network.PacketHandler
import com.teamwizardry.librarianlib.features.sprite.Sprite
import com.teamwizardry.sao.SAO
import com.teamwizardry.sao.api.SAOPlayerData
import com.teamwizardry.sao.common.network.PacketPropose
import com.teamwizardry.sao.common.network.friends.PacketRemoveFriend
import net.minecraft.util.ResourceLocation
import java.util.*
import java.util.function.Consumer


private val iconEquipment = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/canceldisolve.png"))
private val iconEquipmentSelected = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/canceldisolve_activated.png"))
private val iconSkills = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/canceldisolve.png"))
private val iconSkillsSelected = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/canceldisolve_activated.png"))
private val iconItems = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/items.png"))
private val iconItemsSelected = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/items_selected.png"))
private val iconFriends = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/friend.png"))
private val iconFriendsSelected = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/friend_activated.png"))

class CategorySelf(val gui: GuiSAOUI) : ComponentMenu() {

    private val profileComponent = ComponentPlayerProfile(gui, Minecraft().player.uniqueID)

    init {
        add(profileComponent)

        val equipment = ComponentMenuSlot(gui, this, 0, iconEquipment, iconEquipmentSelected, "Equipment", null)
        val items = ComponentMenuSlot(gui, this, 1, iconItems, iconItemsSelected, "Items", ComponentInventory(gui))
        val skills = ComponentMenuSlot(gui, this, 2, iconSkills, iconSkillsSelected, "Skills", null)

        addMenuSlots(equipment, items, skills)

        if (gui.openEquipment) {
            items.onClick()
        }
    }

    override fun wipeIn(): Float {
        profileComponent.wipeIn()

        return super.wipeIn()
    }

    override fun wipeOut(): Float {
        profileComponent.wipeOut()

        return super.wipeOut()
    }
}

class CategoryGuildsFriends(val gui: GuiSAOUI) : ComponentMenu() {

    init {
        var i = 0
        val friendsSlot = ComponentMenuSlot(gui, this, i++, iconFriends, iconFriendsSelected, "Friends", FriendsList(gui))
        val partySlot = ComponentMenuSlot(gui, this, i++, iconEquipment, iconEquipmentSelected, "Party", PartyList(gui))
        val guildSlot = ComponentMenuSlot(gui, this, i, iconSkills, iconSkillsSelected, "Guild", Guild(gui))
        addMenuSlots(friendsSlot, partySlot, guildSlot)
    }

    class Guild(val gui: GuiSAOUI) : ComponentMenu() {
        private var requestsComponent = ComponentGuildManagement(150, 202, gui)

        init {

            add(requestsComponent)

            val myData = GuiSAOUI.getPlayerData(Minecraft().player.uniqueID)
            myData?.let {
                val partyMembers: HashSet<SAOPlayerData> = it.party.members

                for ((i, player) in partyMembers.withIndex()) {

                    val friendSlot = ComponentMenuSlot(gui, this, i, iconFriends, iconFriendsSelected, player.name, FriendOptions(player.uuid, gui))
                    addMenuSlots(friendSlot)
                }
            }
        }

        override fun wipeIn(): Float {
            requestsComponent.wipeIn()

            return super.wipeIn()
        }

        override fun wipeOut(): Float {
            requestsComponent.wipeOut()

            return super.wipeOut()
        }
    }

    class PartyList(val gui: GuiSAOUI) : ComponentMenu() {
        private var requestsComponent = ComponentPartyManagement(150, 202, gui)

        init {

            add(requestsComponent)

            val myData = GuiSAOUI.getPlayerData(Minecraft().player.uniqueID)
            myData?.let {
                val partyMembers: HashSet<SAOPlayerData> = it.party.members

                for ((i, player) in partyMembers.withIndex()) {

                    val friendSlot = ComponentMenuSlot(gui, this, i, iconFriends, iconFriendsSelected, player.name, FriendOptions(player.uuid, gui))
                    addMenuSlots(friendSlot)
                }
            }
        }

        override fun wipeIn(): Float {
            requestsComponent.wipeIn()

            return super.wipeIn()
        }

        override fun wipeOut(): Float {
            requestsComponent.wipeOut()

            return super.wipeOut()
        }
    }

    class FriendsList(val gui: GuiSAOUI) : ComponentMenu() {
        private val requestsComponent = ComponentFriendRequest(gui)

        init {

            add(requestsComponent)

            val myData = GuiSAOUI.getPlayerData(Minecraft().player.uniqueID)
            myData?.let {
                for ((i, friendData) in it.friends.listFriends.withIndex()) {

                    val friendSlot = ComponentMenuSlot(gui, this, i, iconFriends, iconFriendsSelected, friendData.name, FriendOptions(friendData.uuid, gui))
                    addMenuSlots(friendSlot)
                }
            }
        }

        override fun wipeIn(): Float {
            requestsComponent.wipeIn()

            return super.wipeIn()
        }

        override fun wipeOut(): Float {
            requestsComponent.wipeOut()

            return super.wipeOut()
        }
    }

    class FriendOptions(val uuid: UUID, val gui: GuiSAOUI) : ComponentMenu() {

        init {

            val myData = GuiSAOUI.getPlayerData(Minecraft().player.uniqueID)

            var i = 0
            val profile = ComponentMenuSlot(gui, this, i++, iconFriends, iconFriendsSelected,
                    "Profile", ComponentPlayerProfile(gui, uuid), true)
            val message = ComponentMenuSlot(gui, this, i++, iconFriends, iconFriendsSelected,
                    "Message", null)
            val propose = ComponentMenuSlot(gui, this, i++, iconFriends, iconFriendsSelected,
                    if (myData?.marriedTo == uuid) "Divorce" else "Propose", null, onClicked = Consumer {
                myData?.also { it1 ->
                    if (it1.marriedTo == null && it1.proposeTo == null) {
                        PacketHandler.NETWORK.sendToServer(PacketPropose(it1.uuid))
                    }
                }
            })
            val unfriend = ComponentMenuSlot(gui, this, i, iconFriends, iconFriendsSelected,
                    "Unfriend", null, onClicked = Consumer {

                PacketHandler.NETWORK.sendToServer(PacketRemoveFriend(Minecraft().player.uniqueID, uuid))
            })
            addMenuSlots(profile, message, propose, unfriend)
        }
    }
}

class CategoryDuelsMarriageTrading(val gui: GuiSAOUI) : ComponentMenu() {

    init {
        var i = 0
        val duelsSlot = ComponentMenuSlot(gui, this, i++, iconSkills, iconSkillsSelected, "Duels", DuelsMenu(gui))
        val marriageSlot = ComponentMenuSlot(gui, this, i++, iconFriends, iconFriendsSelected, "Marriage", DuelsMenu(gui))
        val tradingSlot = ComponentMenuSlot(gui, this, i, iconFriends, iconFriendsSelected, "Trading", DuelsMenu(gui))
        addMenuSlots(duelsSlot, marriageSlot, tradingSlot)
    }

    class DuelsMenu(val gui: GuiSAOUI) : ComponentMenu() {
        private var requestsComponent = ComponentDuelRequest(gui)

        init {

            add(requestsComponent)
        }

        override fun wipeIn(): Float {
            requestsComponent.wipeIn()

            return super.wipeIn()
        }

        override fun wipeOut(): Float {
            requestsComponent.wipeOut()

            return super.wipeOut()
        }
    }


}