package com.teamwizardry.sao.client.gui

import com.teamwizardry.librarianlib.features.animator.Easing
import com.teamwizardry.librarianlib.features.animator.animations.BasicAnimation
import com.teamwizardry.librarianlib.features.animator.animations.ScheduledEventAnimation
import com.teamwizardry.librarianlib.features.gui.GuiBase
import com.teamwizardry.librarianlib.features.kotlin.Minecraft
import com.teamwizardry.librarianlib.features.sprite.Sprite
import com.teamwizardry.sao.SAO
import com.teamwizardry.sao.api.DataGuild
import com.teamwizardry.sao.api.SAOPlayerData
import com.teamwizardry.sao.client.SAODataClient
import net.minecraft.util.ResourceLocation
import java.util.*

class GuiSAOUI(val openEquipment: Boolean = false) : GuiBase() {

    companion object {
        protected val iconInv = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/inventory.png"))
        protected val iconCommunications = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/communications.png"))
        protected val iconFriends = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/party.png"))
        protected val iconMap = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/map.png"))
        protected val iconSettings = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/settings.png"))

        protected val iconInvSelected = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/inventory_activated.png"))
        protected val iconCommunicationsSelected = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/communications_activated.png"))
        protected val iconFriendsSelected = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/party_activated.png"))
        protected val iconMapSelected = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/map_activated.png"))
        protected val iconSettingsSelected = Sprite(ResourceLocation(SAO.MOD_ID, "textures/ui/settings_activated.png"))

        fun getPlayerData(uuid: UUID = Minecraft().player.uniqueID): SAOPlayerData? {
            return SAODataClient.INSTANCE.getSAOPlayerData(uuid)
        }

        fun getPlayerData(name: String = Minecraft().player.name): SAOPlayerData? {
            return SAODataClient.INSTANCE.getSAOPlayerData(name)
        }

        fun getGuildContainingMember(uuid: UUID = Minecraft().player.uniqueID): DataGuild? {
            return SAODataClient.INSTANCE.getGuildContainingMember(uuid)
        }

        fun getGuildsInvitingMember(uuid: UUID = Minecraft().player.uniqueID): HashSet<DataGuild> {
            return SAODataClient.INSTANCE.getGuildsContainingInviteFor(uuid)
        }
    }

    val categoryButtons: Array<ComponentCategoryButton?> = arrayOfNulls(6)
    val categoryCount = 5
    val categorySize = 32

    init {
        var i = 0
        val categorySelf = ComponentCategoryButton(this, i++, categorySize, categoryButtons.size, main, iconInv, iconInvSelected, CategorySelf(this))
        val categoryFriendsGuild = ComponentCategoryButton(this, i++, categorySize, categoryButtons.size, main, iconFriends, iconFriendsSelected, CategoryGuildsFriends(this))
        val categoryCommunications = ComponentCategoryButton(this, i++, categorySize, categoryButtons.size, main, iconCommunications, iconCommunicationsSelected, CategoryDuelsMarriageTrading(this))
        val categoryMap = ComponentCategoryButton(this, i++, categorySize, categoryButtons.size, main, iconMap, iconMapSelected, null)
        val categorySettings = ComponentCategoryButton(this, i, categorySize, categoryButtons.size, main, iconSettings, iconSettingsSelected, null)
        main.add(categorySelf, categoryFriendsGuild, categoryCommunications, categoryMap, categorySettings)


        // --- POST INIT --- //

        for (j in 0 until categoryButtons.size) {
            val category = categoryButtons[j] ?: continue

            val animPos: BasicAnimation<ComponentCategoryButton> = BasicAnimation(category, "pos.y")
            animPos.duration = j * 5f
            animPos.easing = Easing.easeOutBack
            animPos.to = (main.height / 2) - ((categoryCount * categorySize) / 2) + (j * categorySize)
            category.add(animPos)
        }

        val endStartAnim = ScheduledEventAnimation(5f) {
            if (openEquipment && categorySelf.childComponent is ISlidableComponent)
                categorySelf.childComponent.wipeIn()
        }

        main.add(endStartAnim)
    }

    fun moveTo(x: Int = 0): Float {
        for (j in 0 until categoryButtons.size) {
            val category = categoryButtons[j] ?: continue

            val animPos: BasicAnimation<ComponentCategoryButton> = BasicAnimation(category, "pos.x")
            animPos.duration = 10f + (j * 5f)
            animPos.easing = Easing.easeOutCubic
            animPos.to = (main.width / 2) - (categorySize / 2) - x
            category.add(animPos)
            category.addTag("offset")
        }

        return 10f
    }

    override fun doesGuiPauseGame(): Boolean {
        return false
    }

    fun adjustGuiSize(): Boolean {
        return true
    }
}
