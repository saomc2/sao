package com.teamwizardry.sao.client;

import com.teamwizardry.sao.SAO;
import com.teamwizardry.sao.client.gui.GuiSAOUI;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiInventory;
import net.minecraftforge.client.event.GuiOpenEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@Mod.EventBusSubscriber(modid = SAO.MOD_ID, value = Side.CLIENT)
public class HotbarHandler {

	/*
	@SideOnly(Side.CLIENT)
	@SubscribeEvent
	public static void onScroll(MouseEvent event) {
		EntityPlayer player = Minecraft.getMinecraft().player;
		if (player == null) return;

		if (Keyboard.isCreated() && event.getDwheel() != 0) {

			int slot = Minecraft.getMinecraft().player.inventory.currentItem;
			if (slot >= 4 && event.getDwheel() < 0) {
				Minecraft.getMinecraft().player.inventory.currentItem = 0;
				event.setCanceled(true);
			} else if (slot == 0 && event.getDwheel() > 0) {
				Minecraft.getMinecraft().player.inventory.currentItem = 4;
				event.setCanceled(true);
			}
		}
	}
	*/

	@SideOnly(Side.CLIENT)
	@SubscribeEvent
	public static void onOpenGui(GuiOpenEvent event) {
		if (event.getGui() instanceof GuiInventory && !Minecraft.getMinecraft().player.isSneaking()/* && Minecraft.getMinecraft().player.isCreative()*/) {
			event.setGui(new GuiSAOUI(true));
		}
	}
}
