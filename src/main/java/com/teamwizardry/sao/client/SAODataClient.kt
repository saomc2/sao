package com.teamwizardry.sao.client

import com.teamwizardry.sao.api.DataGuild
import com.teamwizardry.sao.api.SAOPlayerData
import net.minecraft.nbt.NBTTagCompound
import java.util.HashMap
import java.util.UUID
import kotlin.collections.HashSet
import kotlin.collections.set

class SAODataClient private constructor() {

    companion object {
        val INSTANCE = SAODataClient()
    }

    private val playerData = HashMap<UUID, SAOPlayerData>()

    private val rawData = HashMap<UUID, NBTTagCompound>()

    private val guilds = HashSet<DataGuild>()

    fun getSAOPlayerData(uuid: UUID): SAOPlayerData? {
        return playerData[uuid]
    }

    fun getSAOPlayerData(name: String): SAOPlayerData? {
        for (entry in playerData.entries) {
            if (entry.value.name == name) return entry.value
        }
        return null
    }

    fun getGuildsContainingInviteFor(uuid: UUID): HashSet<DataGuild> {
        val invitingGuilds = HashSet<DataGuild>()
        for (guild in guilds) {
            if (guild.invites.contains(uuid)) invitingGuilds.add(guild)
        }

        return invitingGuilds
    }

    fun getGuildContainingMember(uuid: UUID): DataGuild? {
        for (guild in guilds) {
            if (guild.members.contains(uuid)) return guild
        }

        return null
    }

    fun addGuild(guild: DataGuild) {
        if (guilds.contains(guild)) guilds.remove(guild)
        guilds.add(guild)
    }

    fun addSAOPlayerData(data: SAOPlayerData) {
        playerData[data.uuid] = data
    }

    fun storeRawPlayerData(uuid: UUID, nbt: NBTTagCompound) {
        rawData[uuid] = nbt
    }

    fun deserializeRawData() {
        for (entry in rawData) {
            playerData[entry.key] = SAOPlayerData(entry.key)
        }

        for (entry in rawData) {
            playerData[entry.key]?.deserializeNBT(entry.value)
        }
        rawData.clear()
    }

    fun clearData() {
        guilds.clear()
        playerData.clear()
        rawData.clear()
    }
}
