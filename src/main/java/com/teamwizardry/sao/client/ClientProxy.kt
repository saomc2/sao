package com.teamwizardry.sao.client

import com.teamwizardry.librarianlib.features.kotlin.Minecraft
import com.teamwizardry.librarianlib.features.kotlin.sendSpamlessMessage
import com.teamwizardry.sao.client.gui.GuiSAOUI
import com.teamwizardry.sao.common.CommonProxy
import net.minecraft.client.Minecraft
import net.minecraft.client.settings.KeyBinding
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.util.text.ITextComponent
import net.minecraft.util.text.Style
import net.minecraft.util.text.TextComponentTranslation
import net.minecraft.util.text.TextFormatting
import net.minecraft.world.World
import net.minecraftforge.fml.common.event.FMLInitializationEvent
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent
import net.minecraftforge.fml.relauncher.Side
import net.minecraftforge.fml.relauncher.SideOnly

@SideOnly(Side.CLIENT)
class ClientProxy : CommonProxy() {

    override fun preInit(event: FMLPreInitializationEvent) {
        super.preInit(event)

        KeyBindHandler()
    }

    override fun init(event: FMLInitializationEvent) {
        super.init(event)

        Keybinds.register()
    }

    override fun postInit(event: FMLPostInitializationEvent) {
        super.postInit(event)
    }


    override fun openSAOUI() {
        Minecraft.getMinecraft().displayGuiScreen(GuiSAOUI())
    }

    override fun getWorld(): World? {
        return Minecraft().world
    }

    override fun sendMessage(player: EntityPlayer, messageKey: String, formatting: TextFormatting, vararg placeHolders: ITextComponent) {
        val comp = TextComponentTranslation(messageKey, player.displayName, *placeHolders).setStyle(Style().setColor(formatting))
        Minecraft().player.sendSpamlessMessage(comp, messageKey)
    }

    companion object {

        var keyBindings: Array<KeyBinding>? = null
    }
}
