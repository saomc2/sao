package com.teamwizardry.sao.common.network.party

import com.teamwizardry.librarianlib.features.autoregister.PacketRegister
import com.teamwizardry.librarianlib.features.network.PacketBase
import com.teamwizardry.librarianlib.features.saving.Save
import com.teamwizardry.sao.SAO
import com.teamwizardry.sao.api.util.Utils
import net.minecraft.util.text.TextFormatting
import net.minecraftforge.fml.common.FMLCommonHandler
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext
import net.minecraftforge.fml.relauncher.Side
import java.util.*

/**
 * Created by Demoniaque on 8/16/2016.
 */
@PacketRegister(Side.SERVER)
class PacketLeaveParty(@Save var partyLeader: UUID? = null) : PacketBase() {

    override fun handle(ctx: MessageContext) {

        if (partyLeader == null) return

        val member = ctx.serverHandler.player
        val leaderPlayer = FMLCommonHandler.instance().minecraftServerInstance.playerList.getPlayerByUUID(partyLeader!!)
                ?: return

        val memberData = Utils.getPlayerData(leaderPlayer.uniqueID) ?: return
        val leaderData = Utils.getPlayerData(member.uniqueID) ?: return

        if (memberData.party.getLeader() != leaderData) {
            Utils.sendMessage(member, "message.not_in_their_party", TextFormatting.RED, leaderPlayer.displayName)
            return
        }

        if (memberData.party.isLonely()) {
            Utils.sendMessage(member, "message.not_in_a_party", TextFormatting.RED, leaderPlayer.displayName)
            return
        }

        val success = SAO.proxy.getWorld()?.let { memberData.party.removePartyMember(it, memberData) } ?: false

        if (success) {
            Utils.sendMessage(member, "message.leave_party", TextFormatting.GREEN, leaderPlayer.displayName)
            Utils.sendMessage(leaderPlayer, "message.member_left_party", TextFormatting.YELLOW, member.displayName)
        } else Utils.sendMessage(member, "message.something_went_wrong", TextFormatting.RED)
    }
}
