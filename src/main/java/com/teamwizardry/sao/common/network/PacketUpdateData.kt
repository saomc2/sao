package com.teamwizardry.sao.common.network

import com.teamwizardry.librarianlib.core.LibrarianLib
import com.teamwizardry.librarianlib.features.autoregister.PacketRegister
import com.teamwizardry.librarianlib.features.network.PacketBase
import com.teamwizardry.librarianlib.features.saving.Save
import com.teamwizardry.sao.api.util.Utils
import net.minecraft.nbt.NBTTagCompound
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext
import net.minecraftforge.fml.relauncher.Side

/**
 * Created by Demoniaque on 8/16/2016.
 */
@PacketRegister(Side.CLIENT)
class PacketUpdateData : PacketBase {

    @Save
    var compound: NBTTagCompound? = null

    constructor()

    constructor(compound: NBTTagCompound) {
        this.compound = compound
    }

    override fun handle(ctx: MessageContext) {
        if (compound != null) {

            val player = LibrarianLib.PROXY.getClientPlayer()

            Utils.getPlayerData(player.uniqueID)?.deserializeNBT(compound!!)
        }
    }
}
