package com.teamwizardry.sao.common.network;

import com.teamwizardry.librarianlib.core.LibrarianLib;
import com.teamwizardry.librarianlib.features.autoregister.PacketRegister;
import com.teamwizardry.librarianlib.features.network.PacketBase;
import com.teamwizardry.librarianlib.features.saving.Save;
import com.teamwizardry.sao.api.DataFriends;
import com.teamwizardry.sao.api.SAOPlayerData;
import com.teamwizardry.sao.api.util.Utils;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;

import javax.annotation.Nonnull;

/**
 * Created by Demoniaque on 8/16/2016.
 */
@PacketRegister(Side.CLIENT)
public class PacketUpdateFriends extends PacketBase {

	@Save
	public NBTTagCompound compound;

	public PacketUpdateFriends() {
	}

	public PacketUpdateFriends(NBTTagCompound compound) {
		this.compound = compound;
	}

	@Override
	public void handle(@Nonnull MessageContext ctx) {
		if (ctx.side.isServer()) return;

		EntityPlayer player = LibrarianLib.PROXY.getClientPlayer();

		SAOPlayerData holderData = Utils.Companion.getPlayerData(player.getUniqueID());

		if (holderData != null) {
			DataFriends friends = holderData.getFriends();

			friends.deserializeNBT(compound);
		}
	}
}
