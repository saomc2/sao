package com.teamwizardry.sao.common.network

import com.teamwizardry.librarianlib.features.autoregister.PacketRegister
import com.teamwizardry.librarianlib.features.network.PacketBase
import com.teamwizardry.librarianlib.features.network.PacketHandler
import com.teamwizardry.librarianlib.features.saving.Save
import com.teamwizardry.sao.api.DataParty
import com.teamwizardry.sao.api.capability.world.SAOWorldCap
import net.minecraft.entity.player.EntityPlayerMP
import net.minecraftforge.common.DimensionManager
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext
import net.minecraftforge.fml.relauncher.Side
import java.util.*

/**
 * Created by Demoniaque on 8/16/2016.
 */
@PacketRegister(Side.SERVER)
class PacketUpdatePartyClient : PacketBase {

    @Save
    var dataParty: DataParty? = null
    @Save
    var uuid: UUID? = null

    constructor()

    constructor(dataParty: DataParty, uuid: UUID) {
        this.dataParty = dataParty
        this.uuid = uuid
    }

    override fun handle(ctx: MessageContext) {
        if (uuid != null && dataParty != null) {
            val world = DimensionManager.getWorld(0)
            val cap = SAOWorldCap.get(world)
            val data = cap.getSAOPlayerData(uuid!!)
            dataParty?.let {
                data?.party?.deserializeNBT(dataParty!!.serializeNBT())
            }
            world.getPlayerEntityByUUID(uuid!!)?.let {
                PacketHandler.NETWORK.sendTo(PacketUpdateParty(dataParty), it as EntityPlayerMP?)
            }
        }
    }
}
