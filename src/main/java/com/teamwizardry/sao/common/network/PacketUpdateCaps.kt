package com.teamwizardry.sao.common.network

import com.teamwizardry.librarianlib.core.LibrarianLib
import com.teamwizardry.librarianlib.features.autoregister.PacketRegister
import com.teamwizardry.librarianlib.features.network.PacketBase
import com.teamwizardry.librarianlib.features.saving.Save
import com.teamwizardry.librarianlib.features.utilities.client.ClientRunnable
import com.teamwizardry.sao.api.capability.player.SAOPlayerCap
import net.minecraft.nbt.NBTTagCompound
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext
import net.minecraftforge.fml.relauncher.Side

/**
 * Created by Demoniaque on 8/16/2016.
 */
@PacketRegister(Side.CLIENT)
class PacketUpdateCaps : PacketBase {

    @Save
    var tags: NBTTagCompound? = null

    constructor()

    constructor(tag: NBTTagCompound) {
        tags = tag
    }

    override fun handle(ctx: MessageContext) {
        if (ctx.side.isServer) return

        ClientRunnable.run {

            val player = LibrarianLib.PROXY.getClientPlayer()

            val cap = SAOPlayerCap.get(player)

            if (cap != null) {
                cap.deserializeNBT(tags)

                for (i in 0 until cap.favoriteItems.size) {
                    player.inventory.itemStack = cap.favoriteItems[i]
                }
            }
        }

    }
}
