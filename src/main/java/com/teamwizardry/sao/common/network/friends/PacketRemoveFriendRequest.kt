package com.teamwizardry.sao.common.network.friends

import com.teamwizardry.librarianlib.features.autoregister.PacketRegister
import com.teamwizardry.librarianlib.features.network.PacketBase
import com.teamwizardry.librarianlib.features.saving.Save
import com.teamwizardry.sao.SAO
import com.teamwizardry.sao.api.util.Utils
import net.minecraft.util.text.TextFormatting
import net.minecraftforge.fml.common.FMLCommonHandler
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext
import net.minecraftforge.fml.relauncher.Side
import java.util.*

/**
 * Created by Demoniaque on 8/16/2016.
 */
@PacketRegister(Side.SERVER)
class PacketRemoveFriendRequest : PacketBase {

    @Save
    var receiver: UUID? = null
    @Save
    var sender: UUID? = null

    constructor()

    constructor(sender: UUID, receiver: UUID) {
        this.receiver = receiver
        this.sender = sender
    }

    override fun handle(ctx: MessageContext) {
        if (sender == null || receiver == null) return

        val senderPlayer = FMLCommonHandler.instance().minecraftServerInstance.playerList.getPlayerByUUID(sender!!)
                ?: return
        val receiverPlayer = FMLCommonHandler.instance().minecraftServerInstance.playerList.getPlayerByUUID(receiver!!)
                ?: return
        val senderData = Utils.getPlayerData(sender!!) ?: return
        val receiverData = Utils.getPlayerData(receiver!!) ?: return

        val result = SAO.proxy.getWorld()?.let { senderData.friends.removeInvite(it, receiverData) } ?: false

        if (result) {
            Utils.sendMessage(senderPlayer, "message.deny_friend_request", TextFormatting.YELLOW, receiverPlayer.displayName)
            Utils.sendMessage(receiverPlayer, "message.friend_request_denied", TextFormatting.RED, senderPlayer.displayName)
        } else {
            Utils.sendMessage(senderPlayer, "message.friend_request_already_denied", TextFormatting.RED, receiverPlayer.displayName)
        }
    }
}
