package com.teamwizardry.sao.common.network.guild

import com.teamwizardry.librarianlib.features.autoregister.PacketRegister
import com.teamwizardry.librarianlib.features.network.PacketBase
import com.teamwizardry.librarianlib.features.saving.Save
import com.teamwizardry.sao.SAO
import com.teamwizardry.sao.api.capability.world.SAOWorldCap
import com.teamwizardry.sao.api.util.Utils
import net.minecraft.util.text.TextFormatting
import net.minecraftforge.fml.common.FMLCommonHandler
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext
import net.minecraftforge.fml.relauncher.Side
import java.util.*

/**
 * Created by Demoniaque on 8/16/2016.
 */
@PacketRegister(Side.SERVER)
class PacketRedactGuildInvite(@Save var receiver: UUID? = null) : PacketBase() {

    override fun handle(ctx: MessageContext) {

        if (receiver == null) return

        val senderPlayer = ctx.serverHandler.player
        val receiverPlayer = FMLCommonHandler.instance().minecraftServerInstance.playerList.getPlayerByUUID(receiver!!)
                ?: return

        val cap = SAOWorldCap.get(SAO.proxy.getWorld())
        val inviteeGuildInvites = cap.getGuildsContainingInviteFor(receiver!!)
        val leaderGuild = cap.getGuildContainingMember(senderPlayer.uniqueID) ?: return

        if (!inviteeGuildInvites.contains(leaderGuild)) {
            Utils.sendMessage(senderPlayer, "message.guild_invite_already_cancelled", TextFormatting.RED, receiverPlayer.displayName)
            return
        }

        val success = SAO.proxy.getWorld()?.let { leaderGuild.invites.remove(receiver!!) } ?: false

        if (success) {
            Utils.sendMessage(senderPlayer, "message.cancel_guild_invite", TextFormatting.GREEN, receiverPlayer.displayName)
            Utils.sendMessage(receiverPlayer, "message.guild_invite_cancelled", TextFormatting.YELLOW, senderPlayer.displayName)
        } else Utils.sendMessage(senderPlayer, "message.something_went_wrong", TextFormatting.RED)
    }
}
