package com.teamwizardry.sao.common.network.trade

import com.teamwizardry.librarianlib.features.autoregister.PacketRegister
import com.teamwizardry.librarianlib.features.kotlin.Minecraft
import com.teamwizardry.librarianlib.features.network.PacketBase
import com.teamwizardry.librarianlib.features.saving.Save
import com.teamwizardry.sao.SAO
import com.teamwizardry.sao.api.capability.world.SAOWorldCap
import com.teamwizardry.sao.api.util.Utils
import net.minecraft.util.text.TextComponentString
import net.minecraft.util.text.TextFormatting
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext
import net.minecraftforge.fml.relauncher.Side

@PacketRegister(Side.CLIENT)
class PacketSendTradeRequest(@Save var playerName: String? = null) : PacketBase() {

    override fun handle(ctx: MessageContext) {

        if (playerName == null) return

        val invitee = Minecraft().player

        val cap = SAOWorldCap.get(SAO.proxy.getWorld())
        val partner = cap.getCurrentTradingPartner(invitee.uniqueID)

        if (partner != null) {
            Utils.sendMessage(invitee, "message.trade_request", TextFormatting.YELLOW, TextComponentString(playerName))
            println("hi")
        }
    }
}
