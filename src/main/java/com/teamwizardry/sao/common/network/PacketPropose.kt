package com.teamwizardry.sao.common.network

import com.teamwizardry.librarianlib.features.autoregister.PacketRegister
import com.teamwizardry.librarianlib.features.network.PacketBase
import com.teamwizardry.librarianlib.features.saving.Save
import com.teamwizardry.sao.SAO
import com.teamwizardry.sao.api.capability.world.SAOWorldCap
import com.teamwizardry.sao.api.util.Utils
import net.minecraft.util.text.TextFormatting
import net.minecraftforge.fml.common.FMLCommonHandler
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext
import net.minecraftforge.fml.relauncher.Side
import java.util.*

/**
 * Created by Demoniaque on 8/16/2016.
 */
@PacketRegister(Side.SERVER)
class PacketPropose(@Save var uuid: UUID? = null) : PacketBase() {

    override fun handle(ctx: MessageContext) {

        if (this.uuid == null) return

        val world = SAO.proxy.getWorld() ?: return
        val senderPlayer = ctx.serverHandler.player
        val receiverPlayer = FMLCommonHandler.instance().minecraftServerInstance.playerList.getPlayerByUUID(uuid!!)
                ?: return

        val receiverData = Utils.getPlayerData(receiverPlayer.uniqueID) ?: return
        val senderData = Utils.getPlayerData(senderPlayer.uniqueID) ?: return

        if (receiverData.marriedTo != null) {
            Utils.sendMessage(senderPlayer, "message.friend_already_married", TextFormatting.RED, receiverPlayer.displayName)
            return
        }
        if (senderData.marriedTo != null) {
            Utils.sendMessage(senderPlayer, "message.your_already_married", TextFormatting.RED, receiverPlayer.displayName)
            return
        }
        if (receiverData.proposeTo != null) {
            Utils.sendMessage(senderPlayer, "message.friend_has_pending_proposal", TextFormatting.RED, receiverPlayer.displayName)
            return
        }
        if (senderData.proposeTo != null) {
            Utils.sendMessage(senderPlayer, "message.you_have_pending_proposal", TextFormatting.RED, receiverPlayer.displayName)
            return
        }

        for (data in SAOWorldCap.get(world).getAllPlayerData()) {
            if (data.key == uuid || data.key == senderData.uuid) continue
            if (data.value.proposeTo == uuid) {
                Utils.sendMessage(senderPlayer, "message.friend_has_pending_proposal", TextFormatting.RED, receiverPlayer.displayName)
                return
            }

            if (data.value.proposeTo == senderData.uuid) {
                Utils.sendMessage(senderPlayer, "message.you_have_pending_proposal", TextFormatting.RED, receiverPlayer.displayName)
                return
            }
        }

        senderData.proposeTo = uuid

        Utils.sendMessage(senderPlayer, "message.you_proposed_to_friend", TextFormatting.GREEN, receiverPlayer.displayName)
        Utils.sendMessage(receiverPlayer, "message.friend_proposed_to_you", TextFormatting.GREEN, senderPlayer.displayName)
    }
}
