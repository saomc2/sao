package com.teamwizardry.sao.common.network.friends

import com.teamwizardry.librarianlib.features.autoregister.PacketRegister
import com.teamwizardry.librarianlib.features.network.PacketBase
import com.teamwizardry.librarianlib.features.saving.Save
import com.teamwizardry.sao.SAO
import com.teamwizardry.sao.api.util.Utils
import net.minecraft.util.text.TextComponentString
import net.minecraft.util.text.TextFormatting
import net.minecraftforge.fml.common.FMLCommonHandler
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext
import net.minecraftforge.fml.relauncher.Side
import java.util.*

/**
 * Created by Demoniaque on 8/16/2016.
 */
@PacketRegister(Side.SERVER)
class PacketSendFriendInvite : PacketBase {

    @Save
    var name: String? = null
    @Save
    var sender: UUID? = null

    constructor()

    constructor(sender: UUID, name: String) {
        this.name = name
        this.sender = sender
    }

    override fun handle(ctx: MessageContext) {

        if (this.sender == null || name == null) return

        val senderPlayer = FMLCommonHandler.instance().minecraftServerInstance.playerList.getPlayerByUUID(this.sender!!)
                ?: return
        val receiverPlayer = FMLCommonHandler.instance().minecraftServerInstance.playerList.getPlayerByUsername(name!!)

        if (receiverPlayer == null) {
            Utils.sendMessage(senderPlayer, "message.player_doesnt_exist", TextFormatting.RED, TextComponentString(name!!))
        } else {
            val receiverData = Utils.getPlayerData(receiverPlayer.uniqueID) ?: return
            val senderData = Utils.getPlayerData(senderPlayer.uniqueID) ?: return
            if (receiverData.friends.invites.contains(senderData)) {
                Utils.sendMessage(senderPlayer, "message.already_sent_friend_request", TextFormatting.RED, receiverPlayer.displayName)
                return
            }
            if (receiverData.friends.listFriends.contains(senderData)) {
                Utils.sendMessage(senderPlayer, "message.already_friend", TextFormatting.RED, receiverPlayer.displayName)
               return
            }

            val success = SAO.proxy.getWorld()?.let { receiverData.friends.addInvite(it, senderData) } ?: false

            if (success) {
                Utils.sendMessage(senderPlayer, "message.friend_request_sent", TextFormatting.GREEN, receiverPlayer.displayName)
                Utils.sendMessage(receiverPlayer, "message.friend_request_received", TextFormatting.GREEN, senderPlayer.displayName)
            } else Utils.sendMessage(senderPlayer, "message.something_went_wrong", TextFormatting.RED)
        }
    }

}
