package com.teamwizardry.sao.common.network

import com.teamwizardry.librarianlib.features.autoregister.PacketRegister
import com.teamwizardry.librarianlib.features.network.PacketBase
import com.teamwizardry.sao.SAO
import com.teamwizardry.sao.api.SAOPlayerData
import com.teamwizardry.sao.api.capability.world.SAOWorldCap
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.nbt.NBTTagList
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext
import net.minecraftforge.fml.relauncher.Side
import java.util.*

@PacketRegister(Side.SERVER)
class PacketRequestUpdate : PacketBase() {

    override fun reply(ctx: MessageContext): PacketBase? {

        val player = ctx.serverHandler.player
        val world = SAO.proxy.getWorld()
        val cap = SAOWorldCap.get(world)
        val data = cap.getSAOPlayerData(player.uniqueID) ?: return super.reply(ctx)

        val playerData = HashMap<UUID, SAOPlayerData>()

        playerData[data.uuid] = data

        for (friend in data.friends.listFriends) {
            playerData[friend.uuid] = friend
        }

        for (invitee in data.friends.invites) {
            playerData[invitee.uuid] = invitee
        }

        for (member in data.party.members) {
            playerData[member.uuid] = member
        }

        for (players in SAOWorldCap.get(world).getAllPlayerData()) {
            if (players.value.proposeTo == player.uniqueID || data.proposeTo == players.key) {
                playerData[players.key] = players.value
                continue
            }

            if (players.value.marriedTo == player.uniqueID || data.marriedTo == players.key) {
                playerData[players.key] = players.value
            }
        }

        val list = NBTTagList()
        for (entry in playerData) {
            val nbt = NBTTagCompound()
            nbt.setString("uuid", entry.key.toString())
            nbt.setTag("data", entry.value.serializeNBT())
            list.appendTag(nbt)
        }

        val guilds = NBTTagList()
        for (guild in cap.getGuildsContainingInviteFor(data.uuid)) {
            guilds.appendTag(guild.serializeNBT())
        }

        cap.getGuildContainingMember(player.uniqueID)?.let {
            guilds.appendTag(it.serializeNBT())
        }


        return PacketUpdate(list, guilds)
    }

    override fun handle(ctx: MessageContext) {

    }
}
