package com.teamwizardry.sao.common.network

import com.teamwizardry.librarianlib.features.autoregister.PacketRegister
import com.teamwizardry.librarianlib.features.network.PacketBase
import com.teamwizardry.librarianlib.features.saving.Save
import com.teamwizardry.sao.api.SAOPlayerData
import com.teamwizardry.sao.client.SAODataClient
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.nbt.NBTTagList
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext
import net.minecraftforge.fml.relauncher.Side
import java.util.*

@PacketRegister(Side.CLIENT)
class PacketSendPlayersToClient : PacketBase {

    @Save
    var nbt: NBTTagList? = null

    constructor()

    constructor(datas: Set<SAOPlayerData>) {
        val nbt = NBTTagList()

        datas.forEach {
            val compound = NBTTagCompound()
            compound.setString("uuid", it.uuid.toString())
            compound.setTag("data", it.serializeNBT())
            nbt.appendTag(compound)
        }

        this.nbt = nbt
    }

    override fun handle(ctx: MessageContext) {
        if (ctx.side.isServer) return
        if (nbt == null) return

        for (base in nbt!!) {
            if (base is NBTTagCompound) {
                val uuid = UUID.fromString(base.getString("uuid"))
                val data = base.getCompoundTag("data")

                val playerData = SAOPlayerData.create(uuid, data)
                SAODataClient.INSTANCE.addSAOPlayerData(playerData)
            }
        }
    }
}
