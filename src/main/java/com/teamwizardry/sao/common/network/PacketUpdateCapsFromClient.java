package com.teamwizardry.sao.common.network;

import com.teamwizardry.librarianlib.features.autoregister.PacketRegister;
import com.teamwizardry.librarianlib.features.network.PacketBase;
import com.teamwizardry.librarianlib.features.saving.Save;
import com.teamwizardry.sao.api.capability.player.ISAOPlayer;
import com.teamwizardry.sao.api.capability.player.SAOPlayerCap;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;

import javax.annotation.Nonnull;
import java.util.UUID;

/**
 * Created by Demoniaque on 8/16/2016.
 */
@PacketRegister(Side.SERVER)
public class PacketUpdateCapsFromClient extends PacketBase {

	@Save
	public NBTTagCompound tags;
	@Save
	private UUID uuid;

	public PacketUpdateCapsFromClient() {
	}

	public PacketUpdateCapsFromClient(NBTTagCompound tag, UUID uuid) {
		tags = tag;
		this.uuid = uuid;
	}

	@Override
	public void handle(@Nonnull MessageContext ctx) {
		if (ctx.side.isClient()) return;

		EntityPlayer player = FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayerByUUID(uuid);

		ISAOPlayer cap = SAOPlayerCap.get(player);

		if (cap != null) {
			cap.deserializeNBT(tags);

			for (int i = 0; i < cap.getFavoriteItems().length; i++) {
				player.inventory.setInventorySlotContents(i, cap.getFavoriteItems()[i]);
			}
		}
	}
}
