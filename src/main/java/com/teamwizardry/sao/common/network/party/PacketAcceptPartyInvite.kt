package com.teamwizardry.sao.common.network.party

import com.teamwizardry.librarianlib.features.autoregister.PacketRegister
import com.teamwizardry.librarianlib.features.network.PacketBase
import com.teamwizardry.librarianlib.features.saving.Save
import com.teamwizardry.sao.SAO
import com.teamwizardry.sao.api.util.Utils
import net.minecraft.util.text.TextFormatting
import net.minecraftforge.fml.common.FMLCommonHandler
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext
import net.minecraftforge.fml.relauncher.Side
import java.util.*

/**
 * Created by Demoniaque on 8/16/2016.
 */
@PacketRegister(Side.SERVER)
class PacketAcceptPartyInvite(@Save var partyLeader: UUID? = null) : PacketBase() {

    override fun handle(ctx: MessageContext) {

        if (partyLeader == null) return

        val invitee = ctx.serverHandler.player
        val leaderPlayer = FMLCommonHandler.instance().minecraftServerInstance.playerList.getPlayerByUUID(partyLeader!!)
                ?: return

        val inviteeData = Utils.getPlayerData(invitee.uniqueID) ?: return
        val leaderData = Utils.getPlayerData(leaderPlayer.uniqueID) ?: return

        if (!inviteeData.partyInvites.contains(partyLeader!!)) {
            Utils.sendMessage(invitee, "message.not_invited_to_party", TextFormatting.RED, leaderPlayer.displayName)
            return
        }

        val success = SAO.proxy.getWorld()?.let {
            inviteeData.acceptPartyInvite(it, leaderData.party)
        } ?: false

        if (success) {
            Utils.sendMessage(invitee, "message.accept_party_invite", TextFormatting.GREEN, leaderPlayer.displayName)
            Utils.sendMessage(leaderPlayer, "message.party_invite_accepted", TextFormatting.GREEN, invitee.displayName)
        } else Utils.sendMessage(invitee, "message.something_went_wrong", TextFormatting.RED)
    }
}
