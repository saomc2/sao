package com.teamwizardry.sao.common.network

import com.teamwizardry.librarianlib.features.autoregister.PacketRegister
import com.teamwizardry.librarianlib.features.network.PacketBase
import com.teamwizardry.librarianlib.features.saving.Save
import com.teamwizardry.sao.api.SAOPlayerData
import com.teamwizardry.sao.client.SAODataClient
import net.minecraft.nbt.NBTTagCompound
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext
import net.minecraftforge.fml.relauncher.Side
import java.util.*

@PacketRegister(Side.CLIENT)
class PacketSendPlayerToClient : PacketBase {

    @Save
    var nbt: NBTTagCompound? = null
    @Save
    var uuid: UUID? = null

    constructor()

    constructor(uuid: UUID, nbt: NBTTagCompound) {
        this.nbt = nbt
        this.uuid = uuid
    }

    override fun handle(ctx: MessageContext) {
        if (ctx.side.isServer) return

        if (uuid != null && nbt != null) {
            val data = SAOPlayerData.create(uuid!!, nbt!!)
            SAODataClient.INSTANCE.addSAOPlayerData(data)
        }
    }
}
