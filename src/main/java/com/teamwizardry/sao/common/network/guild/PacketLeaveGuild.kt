package com.teamwizardry.sao.common.network.guild

import com.teamwizardry.librarianlib.features.autoregister.PacketRegister
import com.teamwizardry.librarianlib.features.network.PacketBase
import com.teamwizardry.librarianlib.features.saving.Save
import com.teamwizardry.sao.SAO
import com.teamwizardry.sao.api.capability.world.SAOWorldCap
import com.teamwizardry.sao.api.util.Utils
import net.minecraft.util.text.TextFormatting
import net.minecraftforge.fml.common.FMLCommonHandler
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext
import net.minecraftforge.fml.relauncher.Side
import java.util.*

/**
 * Created by Demoniaque on 8/16/2016.
 */
@PacketRegister(Side.SERVER)
class PacketLeaveGuild(@Save var guildUUID: UUID? = null) : PacketBase() {

    override fun handle(ctx: MessageContext) {

        if (guildUUID == null) return

        val member = ctx.serverHandler.player

        val cap = SAOWorldCap.get(SAO.proxy.getWorld())
        val inviteeGuild = cap.getGuildContainingMember(member.uniqueID) ?: return
        val leaderGuild = cap.getGuildFromUUID(guildUUID!!) ?: return

        val leaderPlayer = FMLCommonHandler.instance().minecraftServerInstance.playerList.getPlayerByUUID(leaderGuild.leader)

        if (inviteeGuild.guildUUID!= leaderGuild.guildUUID) {
            Utils.sendMessage(member, "message.not_in_their_guild", TextFormatting.RED, leaderPlayer.displayName)
            return
        }

        if (inviteeGuild.isLonely()) {
            Utils.sendMessage(member, "message.not_in_a_guild", TextFormatting.RED, leaderPlayer.displayName)
            return
        }

        val success = SAO.proxy.getWorld()?.let { leaderGuild.removeGuildMember(member.uniqueID) } ?: false

        if (success) {
            Utils.sendMessage(member, "message.leave_guild", TextFormatting.GREEN, leaderPlayer.displayName)
            Utils.sendMessage(leaderPlayer, "message.member_left_guild", TextFormatting.YELLOW, member.displayName)
        } else Utils.sendMessage(member, "message.something_went_wrong", TextFormatting.RED)
    }
}
