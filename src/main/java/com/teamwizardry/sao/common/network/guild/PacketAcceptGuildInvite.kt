package com.teamwizardry.sao.common.network.guild

import com.teamwizardry.librarianlib.features.autoregister.PacketRegister
import com.teamwizardry.librarianlib.features.network.PacketBase
import com.teamwizardry.librarianlib.features.saving.Save
import com.teamwizardry.sao.SAO
import com.teamwizardry.sao.api.capability.world.SAOWorldCap
import com.teamwizardry.sao.api.util.Utils
import net.minecraft.util.text.TextFormatting
import net.minecraftforge.fml.common.FMLCommonHandler
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext
import net.minecraftforge.fml.relauncher.Side
import java.util.*

/**
 * Created by Demoniaque on 8/16/2016.
 */
@PacketRegister(Side.SERVER)
class PacketAcceptGuildInvite(@Save var guildUUID: UUID? = null) : PacketBase() {

    override fun handle(ctx: MessageContext) {

        if (guildUUID == null) return

        val invitee = ctx.serverHandler.player

        val cap = SAOWorldCap.get(SAO.proxy.getWorld())
        val inviteeGuildInvites = cap.getGuildsContainingInviteFor(invitee.uniqueID)
        val leaderGuild = cap.getGuildFromUUID(guildUUID!!) ?: return

        val leaderPlayer = FMLCommonHandler.instance().minecraftServerInstance.playerList.getPlayerByUUID(leaderGuild.leader)

        if (!inviteeGuildInvites.contains(leaderGuild)) {
            Utils.sendMessage(invitee, "message.not_invited_to_guild", TextFormatting.RED, leaderPlayer.displayName)
            return
        }

        val success = SAO.proxy.getWorld()?.let {
            leaderGuild.addGuildMember(it, invitee.uniqueID)
        } ?: false

        if (success) {
            Utils.sendMessage(invitee, "message.accept_guild_invite", TextFormatting.GREEN, leaderPlayer.displayName)
            Utils.sendMessage(leaderPlayer, "message.guild_invite_accepted", TextFormatting.GREEN, invitee.displayName)
        } else Utils.sendMessage(invitee, "message.something_went_wrong", TextFormatting.RED)
    }
}
