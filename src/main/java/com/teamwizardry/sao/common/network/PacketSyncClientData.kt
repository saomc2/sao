package com.teamwizardry.sao.common.network

import com.teamwizardry.librarianlib.features.autoregister.PacketRegister
import com.teamwizardry.librarianlib.features.network.PacketBase
import com.teamwizardry.librarianlib.features.saving.Save
import com.teamwizardry.sao.SAO
import com.teamwizardry.sao.api.DataGuild
import com.teamwizardry.sao.client.SAODataClient
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.nbt.NBTTagList
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext
import net.minecraftforge.fml.relauncher.Side
import java.util.*

@PacketRegister(Side.CLIENT)
class PacketSyncClientData : PacketBase {

    @Save
    var list: NBTTagList? = null
    @Save
    var guilds: NBTTagList? = null

    constructor()

    constructor(list: NBTTagList, guilds: NBTTagList) {
        this.list = list
        this.guilds = guilds
    }

    override fun handle(ctx: MessageContext) {
        if (list == null || guilds == null) return

        SAODataClient.INSTANCE.clearData()
        for (base in list!!) {
            if (base is NBTTagCompound) {
                if (base.hasKey("uuid") && base.hasKey("data")) {
                    val uuid = UUID.fromString(base.getString("uuid"))
                    val dataNBT = base.getCompoundTag("data")

                    SAODataClient.INSTANCE.storeRawPlayerData(uuid, dataNBT)
                }
            }
        }

        for (base in guilds!!) {
            if (base is NBTTagCompound) {
                SAODataClient.INSTANCE.addGuild(DataGuild.createGuild(base))
            }
        }

        SAODataClient.INSTANCE.deserializeRawData()

        SAO.proxy.openSAOUI()
    }
}
