package com.teamwizardry.sao.common.network.party

import com.teamwizardry.librarianlib.features.autoregister.PacketRegister
import com.teamwizardry.librarianlib.features.network.PacketBase
import com.teamwizardry.librarianlib.features.saving.Save
import com.teamwizardry.sao.SAO
import com.teamwizardry.sao.api.util.Utils
import net.minecraft.util.text.TextFormatting
import net.minecraftforge.fml.common.FMLCommonHandler
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext
import net.minecraftforge.fml.relauncher.Side
import java.util.*

/**
 * Created by Demoniaque on 8/16/2016.
 */
@PacketRegister(Side.SERVER)
class PacketSendPartyInvite(@Save var receiver: UUID? = null, @Save var partyName: String = "<NULL>") : PacketBase() {

    override fun handle(ctx: MessageContext) {

        if (receiver == null) return

        val senderPlayer = ctx.serverHandler.player
        val receiverPlayer = FMLCommonHandler.instance().minecraftServerInstance.playerList.getPlayerByUUID(receiver!!)
                ?: return

        val receiverData = Utils.getPlayerData(receiverPlayer.uniqueID) ?: return
        val senderData = Utils.getPlayerData(senderPlayer.uniqueID) ?: return
        senderData.party.name = partyName

        if (receiverData.partyInvites.contains(senderData.party.getLeader().uuid)) {
            Utils.sendMessage(senderPlayer, "message.already_sent_party_request", TextFormatting.RED, receiverPlayer.displayName)
            return
        }
        if (receiverData.party.members.contains(senderData)) {
         //   Utils.sendMessage(senderPlayer, "message.already_in_your_party", TextFormatting.RED, receiverPlayer.displayName)
         //   return
        }
        if (!receiverData.friends.listFriends.contains(senderData)) {
            Utils.sendMessage(senderPlayer, "message.not_your_friend", TextFormatting.RED, receiverPlayer.displayName)
            return
        }

        val success = SAO.proxy.getWorld()?.let { receiverData.partyInvites.add(senderData.party.getLeader().uuid) } ?: false

        if (success) {
            Utils.sendMessage(senderPlayer, "message.party_invite_sent", TextFormatting.GREEN, receiverPlayer.displayName)
            Utils.sendMessage(receiverPlayer, "message.party_invite_received", TextFormatting.GREEN, senderPlayer.displayName)
        } else Utils.sendMessage(senderPlayer, "message.something_went_wrong", TextFormatting.RED)
    }
}
