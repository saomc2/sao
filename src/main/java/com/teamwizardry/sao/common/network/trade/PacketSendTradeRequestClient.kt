package com.teamwizardry.sao.common.network.trade

import com.teamwizardry.librarianlib.features.autoregister.PacketRegister
import com.teamwizardry.librarianlib.features.network.PacketBase
import com.teamwizardry.librarianlib.features.network.PacketHandler
import com.teamwizardry.librarianlib.features.saving.Save
import net.minecraft.entity.player.EntityPlayerMP
import net.minecraftforge.common.DimensionManager
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext
import net.minecraftforge.fml.relauncher.Side
import java.util.*

/**
 * Created by Demoniaque on 8/16/2016.
 */
@PacketRegister(Side.SERVER)
class PacketSendTradeRequestClient : PacketBase {

    @Save
    var playerName: String? = null
    @Save
    var uuid: UUID? = null

    constructor()

    constructor(playerName: String, uuid: UUID) {
        this.playerName = playerName
        this.uuid = uuid
    }

    override fun handle(ctx: MessageContext) {
        if (uuid != null && playerName != null) {
            val world = DimensionManager.getWorld(0)
            world.getPlayerEntityByUUID(uuid!!)?.let {
                PacketHandler.NETWORK.sendTo(PacketSendTradeRequest(playerName), it as EntityPlayerMP?)
            }
        }
    }
}
