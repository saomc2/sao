package com.teamwizardry.sao.common

import com.teamwizardry.sao.SAO
import com.teamwizardry.sao.api.capability.player.SAOPlayerCap
import com.teamwizardry.sao.api.capability.world.SAOWorldCap
import com.teamwizardry.sao.client.SAOGuiHandler
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.util.text.ITextComponent
import net.minecraft.util.text.TextFormatting
import net.minecraft.world.World
import net.minecraftforge.common.DimensionManager
import net.minecraftforge.fml.common.event.FMLInitializationEvent
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent
import net.minecraftforge.fml.common.network.NetworkRegistry

open class CommonProxy {

    open fun preInit(event: FMLPreInitializationEvent) {

        NetworkRegistry.INSTANCE.registerGuiHandler(SAO.instance, SAOGuiHandler())
        SAOWorldCap.init()
        SAOPlayerCap.init()

        //EventHandler
    }

    open fun init(event: FMLInitializationEvent) {

    }

    open fun postInit(event: FMLPostInitializationEvent) {

    }

    open fun openSAOUI() {

    }

    open fun getWorld(): World? = DimensionManager.getWorld(0)

    open fun sendMessage(player: EntityPlayer, messageKey: String, formatting: TextFormatting = TextFormatting.YELLOW, vararg placeHolders: ITextComponent) {
        // noop
    }
}
