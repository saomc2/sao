package com.teamwizardry.sao.common.command

import com.teamwizardry.librarianlib.features.kotlin.sendSpamlessMessage
import com.teamwizardry.sao.api.util.PositionUtils
import com.teamwizardry.sao.api.util.Utils
import net.minecraft.command.CommandBase
import net.minecraft.command.EntitySelector
import net.minecraft.command.ICommandSender
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.entity.player.EntityPlayerMP
import net.minecraft.server.MinecraftServer
import net.minecraft.util.math.BlockPos
import net.minecraft.util.text.ITextComponent
import net.minecraft.util.text.Style
import net.minecraft.util.text.TextComponentString
import net.minecraft.util.text.event.ClickEvent
import net.minecraft.util.text.event.HoverEvent


object CommandDuelRequest : CommandBase() {
    override fun getName(): String {
        return "duel"
    }

    override fun execute(server: MinecraftServer, sender: ICommandSender, args: Array<out String>) {
        if(args.size != 2 || sender.commandSenderEntity == null) {
            sender.sendMessage(TextComponentString(getUsage(sender)))
            return
        }
        val player = args[0]
        val mode = args[1]
        if(player.isBlank()) {
            sender.sendMessage(TextComponentString(getUsage(sender)))
            return
        }
        if(mode.toLowerCase() !in DuelType.values().map { it.toString().toLowerCase() })  {
            when(mode.toLowerCase()) {
                "accept" -> {
                    val other = getPlayer(server, sender, player)
                    val receiverData = Utils.getPlayerData(other.uniqueID) ?: return
                    if(receiverData.challenged == sender.commandSenderEntity?.uniqueID) {
                        if(other.position.distanceSq(sender.commandSenderEntity!!.position) > duelRange* duelRange) {
                            (sender.commandSenderEntity as EntityPlayer).sendSpamlessMessage("§cYou are too far from that player! Get closer to them to duel them.", "duel_request2")
                            return
                        }
                        other.sendSpamlessMessage("§a${sender.name} accepted your duel request!", "duel_response")
                        receiverData.challenged = null
                        (sender.commandSenderEntity as EntityPlayer).sendSpamlessMessage("§aAccepted duel request!", "duel_request_other_side")
                        receiverData.dueling = sender.commandSenderEntity?.uniqueID
                        val otherData = Utils.getPlayerData(sender.commandSenderEntity!!.uniqueID) ?: return
                        otherData.dueling = sender.commandSenderEntity?.uniqueID
                        val x = sender.commandSenderEntity!!.posX + other.posX
                        val y = sender.commandSenderEntity!!.posY + other.posY
                        val z = sender.commandSenderEntity!!.posZ + other.posZ
                        val poz = BlockPos(x/2, y/2, z/2)
                        otherData.posDuel = poz
                        receiverData.posDuel = poz
                        otherData.duelingMode = receiverData.challengedMode
                        receiverData.duelingMode = receiverData.challengedMode
                        receiverData.challengedMode = null
                    }
                }
                "deny" -> {
                    val receiverData = Utils.getPlayerData(getPlayer(server, sender, player).uniqueID) ?: return
                    if(receiverData.challenged == sender.commandSenderEntity?.uniqueID) {
                        getPlayer(server, sender, player).sendSpamlessMessage("§c${sender.name} denied your duel request!", "duel_response")
                        receiverData.challenged = null
                        (sender.commandSenderEntity as EntityPlayerMP).sendSpamlessMessage("§cDenied duel request.", "duel_request_other_side")
                    }
                }
                else -> {
                    sender.sendMessage(TextComponentString(getUsage(sender)))
                    return
                }
            }
        } else {
            if(player == sender.name) {
                sender.sendMessage(TextComponentString(getUsage(sender)))
                return
            }
            val mode = DuelType.values().first { it.name.toLowerCase() == mode.toLowerCase() }
            initiateDuel(sender.commandSenderEntity as EntityPlayerMP, getPlayer(server, sender, player), mode)
        }


    }


    var duelRange: Int = 8; // currently unchanging

    private fun initiateDuel(initiator: EntityPlayerMP, challenged: EntityPlayerMP, mode: DuelType) {
        if(initiator.position.distanceSq(challenged.position) > duelRange* duelRange) {
            initiator.sendSpamlessMessage("§cYou are too far from that player! Get closer to them to challenge them for a duel.", "duel_request")
            return
        }
        val initiatorData = Utils.getPlayerData(initiator.uniqueID) ?: return
        if(initiatorData.challenged != null) {
            initiator.sendSpamlessMessage("§cYou already challenged a player!", "duel_request")
            return
        }
        if(initiatorData.dueling != null) {
            initiator.sendSpamlessMessage("§cYou are already in a duel!", "duel_request")
            return
        }
        val challengedData = Utils.getPlayerData(initiator.uniqueID) ?: return
        if(challengedData.dueling != null) {
            initiator.sendSpamlessMessage("§c${challenged.displayNameString} is already in a duel!", "duel_request")
            return
        }
        if(!PositionUtils.hasProperArena(initiator.world, initiator.position, duelRange) || !PositionUtils.hasProperArena(challenged.world, challenged.position, duelRange)) {
            initiator.sendSpamlessMessage("§cThe area around you isn't ready for a duel, there are blocks in the way!", "duel_request")
            return
        }

        initiator.sendSpamlessMessage("§bInitiating duel with ${challenged.name}...", "duel_request")
        challenged.sendSpamlessMessage(TextComponentString("§bYou have a duel challenge from ${initiator.name} in ")
                .appendSibling(mode.component)
                .appendSibling(TextComponentString("§b... Will you accept? "))
                .appendSibling(TextComponentString("§a[Yes]  ")
                        .setStyle(Style()
                                .setClickEvent(ClickEvent(ClickEvent.Action.RUN_COMMAND, "/duel ${initiator.name} accept"))
                                .setHoverEvent(HoverEvent(HoverEvent.Action.SHOW_TEXT, TextComponentString("Accept the duel request from ${initiator.name}")))
                        ))
                .appendSibling(TextComponentString("§c[No]")
                        .setStyle(Style()
                                .setClickEvent(ClickEvent(ClickEvent.Action.RUN_COMMAND, "/duel ${initiator.name} deny"))
                                .setHoverEvent(HoverEvent(HoverEvent.Action.SHOW_TEXT, TextComponentString("Deny the duel request from ${initiator.name}")))
                        )),
                "duel_request_other_side")
        initiatorData.challenged = challenged.uniqueID
        initiatorData.challengedMode = mode

    }



    override fun getUsage(sender: ICommandSender?): String {
        return ""//"§c/duel <player> <"+DuelType.values().joinToString("/").toLowerCase()+">"
    }

    override fun checkPermission(server: MinecraftServer?, sender: ICommandSender?): Boolean {
        return true
    }

    enum class DuelType(val component: ITextComponent) {
        HALF(TextComponentString("§lFirst to Half§r").setStyle(Style().setHoverEvent(HoverEvent(HoverEvent.Action.SHOW_TEXT, TextComponentString("The winner is decided after the other player loses half of their hit points"))))),
        FIRST(TextComponentString("§lFirst Hit§r").setStyle(Style().setHoverEvent(HoverEvent(HoverEvent.Action.SHOW_TEXT, TextComponentString("The winner is decided by whoever lands the first hit")))))
    }

    override fun getTabCompletions(server: MinecraftServer?, sender: ICommandSender, args: Array<out String>, targetPos: BlockPos?): MutableList<String> {
        return EntitySelector.getPlayers(sender, args[0]).map { it.name }.toMutableList()
    }


}