package com.teamwizardry.sao.common.core

import com.teamwizardry.librarianlib.features.kotlin.Minecraft
import com.teamwizardry.librarianlib.features.kotlin.sendSpamlessMessage
import com.teamwizardry.librarianlib.features.network.PacketHandler
import com.teamwizardry.librarianlib.features.sprite.Sprite
import com.teamwizardry.sao.SAO
import com.teamwizardry.sao.api.capability.world.SAOWorldCap
import com.teamwizardry.sao.api.util.Utils
import com.teamwizardry.sao.client.playerheads.PlayerHeadIcon
import com.teamwizardry.sao.common.command.CommandDuelRequest
import com.teamwizardry.sao.common.network.PacketRequestUpdate
import com.teamwizardry.sao.common.network.PacketUpdateTradingClient
import com.teamwizardry.sao.common.network.trade.PacketSendTradeRequestClient
import net.minecraft.client.renderer.EntityRenderer
import net.minecraft.client.renderer.GlStateManager
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.util.ResourceLocation
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.Vec3d
import net.minecraftforge.client.event.EntityViewRenderEvent
import net.minecraftforge.client.event.RenderGameOverlayEvent
import net.minecraftforge.event.entity.EntityJoinWorldEvent
import net.minecraftforge.event.entity.living.LivingAttackEvent
import net.minecraftforge.event.entity.living.LivingDamageEvent
import net.minecraftforge.event.entity.player.PlayerInteractEvent
import net.minecraftforge.fml.common.Mod
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent
import net.minecraftforge.fml.common.gameevent.TickEvent
import net.minecraftforge.fml.relauncher.Side
import net.minecraftforge.fml.relauncher.SideOnly
import java.awt.Color
import kotlin.math.roundToInt

@Mod.EventBusSubscriber(modid=SAO.MOD_ID)
object EventHandler {
    init {
        //MinecraftForge.EVENT_BUS.register(this)
    }

    @SubscribeEvent
    @JvmStatic
    fun onLogin(event: EntityJoinWorldEvent) {
        if (event.entity !is EntityPlayer) return
        val world = event.world
        val cap = SAOWorldCap.get(world)
        cap.addSAOPlayerData(event.entity as EntityPlayer)
     //   val data = cap.getSAOPlayerData(event.entity.uniqueID)

       // data?.let {
       //     PacketHandler.NETWORK.sendToAll(PacketSendPlayerToClient(event.player.uniqueID, data.serializeNBT()))
       // }
//
       // val datas = HashSet<SAOPlayerData>()
       // for (entry in cap.getAllPlayerData()) {
       //     FMLCommonHandler.instance().minecraftServerInstance.playerList.getPlayerByUUID(entry.key) ?: continue
       //     datas.add(entry.value)
       // }
//
       // PacketHandler.NETWORK.sendTo(PacketSendPlayersToClient(datas), event.player as EntityPlayerMP?)
    }

    @SubscribeEvent
    @JvmStatic
    fun onTradeRequest(event: PlayerInteractEvent.EntityInteract) {
        if (event.target !is EntityPlayer) return
        PacketHandler.NETWORK.sendToServer(PacketSendTradeRequestClient(event.entity.displayName.unformattedText, event.target.uniqueID))
        PacketHandler.NETWORK.sendToServer(PacketUpdateTradingClient(event.entity.displayName.unformattedText, event.target.uniqueID))
        PacketHandler.NETWORK.sendToServer(PacketUpdateTradingClient(event.target.displayName.unformattedText, event.entity.uniqueID))

    }

    @SubscribeEvent
    @JvmStatic
    fun onWorldTick(event: TickEvent.WorldTickEvent) {
        if(event.world.isRemote) return
        for(player in event.world.playerEntities) {
            val data = Utils.getPlayerData(player.uniqueID) ?: continue
            if(data.dueling != null) keepInsideArena(player, data.posDuel!!)
        }
    }

    @SubscribeEvent
    @JvmStatic
    fun onAttack(event: LivingAttackEvent) {
        if(event.entity.world.isRemote) return
        if(event.source.immediateSource is EntityPlayer) {
            val source = event.source.immediateSource as EntityPlayer
            val data = Utils.getPlayerData(source.uniqueID) ?: return
            if(data.dueling == null) return
            val dueling = data.dueling!!
            if(dueling != event.entity.uniqueID) event.isCanceled = true
        } else if(event.entity is EntityPlayer) {
            val attacked = event.entity as EntityPlayer
            val data = Utils.getPlayerData(attacked.uniqueID) ?: return
            if(data.dueling != null) event.isCanceled = true
        }

    }

    @SubscribeEvent
    @JvmStatic
    fun onDamage(event: LivingDamageEvent) {
        if(event.entity.world.isRemote || event.entity !is EntityPlayer) return
        val data = Utils.getPlayerData(event.entity.uniqueID) ?: return
        if(data.dueling == null) return
        if(event.source.immediateSource is EntityPlayer) {
            val sourceData = Utils.getPlayerData(event.source.immediateSource!!.uniqueID) ?: return
            if(sourceData.dueling != event.entity.uniqueID) {
                event.isCanceled = true
                println(event.source.immediateSource!!.uniqueID.toString() + "+++"+data.dueling)
            }
            else {
                when(data.duelingMode!!) {
                    CommandDuelRequest.DuelType.HALF -> {
                        if(event.entityLiving!!.health - event.amount < (event.entityLiving.maxHealth / 2.0)) {
                            val winner = event.source.trueSource as EntityPlayer
                            val loser = event.entity as EntityPlayer
                            winner.sendSpamlessMessage("§aYou won the duel against ${loser.name}!", "duel_result")
                            loser.sendSpamlessMessage("§cYou lost the duel against ${loser.name}!", "duel_result")
                            val dataWinner = Utils.getPlayerData(winner.uniqueID) ?: return
                            val dataLoser = Utils.getPlayerData(loser.uniqueID) ?: return
                            dataWinner.duelingMode = null
                            dataWinner.dueling = null
                            dataLoser.duelingMode = null
                            dataLoser.dueling = null
                        }
                    }
                    CommandDuelRequest.DuelType.FIRST -> {
                        val winner = event.source.trueSource as EntityPlayer
                        val loser = event.entity as EntityPlayer
                        winner.sendSpamlessMessage("§aYou won the duel against ${loser.name}!", "duel_result")
                        loser.sendSpamlessMessage("§cYou won the duel against ${loser.name}!", "duel_result")
                        val dataWinner = Utils.getPlayerData(winner.uniqueID) ?: return
                        val dataLoser = Utils.getPlayerData(loser.uniqueID) ?: return
                        dataWinner.duelingMode = null
                        dataWinner.dueling = null
                        dataLoser.duelingMode = null
                        dataLoser.dueling = null
                    }
                }
            }
        } else event.isCanceled
    }

    /**
     * The next two functions are copied basically one to one from EntityDoppleganger
     * in the Botania code by Vazkii, with accordance with the Botania license.
     */

    fun pointDistanceSpace(x1: Double, y1: Double, z1: Double, x2: Double, y2: Double, z2: Double): Float {
        return Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2) + (z1 - z2) * (z1 - z2)).toFloat()
    }

    private fun keepInsideArena(player: EntityPlayer, source: BlockPos) {
        if (pointDistanceSpace(player.posX, player.posY, player.posZ, source.getX() + 0.5, source.getY() + 0.5, source.getZ() + 0.5) >= CommandDuelRequest.duelRange) {
            val sourceVector = Vec3d(source.getX() + 0.5, source.getY() + 0.5, source.getZ() + 0.5)
            val playerVector = Vec3d(player.posX, player.posY - player.yOffset - player.height / 2, player.posZ)
            val motion = sourceVector.subtract(playerVector).normalize()

            player.motionX = motion.x
            player.motionY = 0.2
            player.motionZ = motion.z
            player.velocityChanged = true
        }
    }

    @SideOnly(Side.CLIENT)
    @SubscribeEvent
    @JvmStatic
    fun renderEntity(entityViewRenderEvent: EntityViewRenderEvent) {
        val entity = entityViewRenderEvent.entity
        EntityRenderer.drawNameplate(Minecraft().fontRenderer, entity.name, entity.posX.toFloat(), entity.posY.toFloat() + 3, entity.posZ.toFloat(), 0, Minecraft().player.rotationYaw, Minecraft().player.rotationPitch, false, Minecraft().player.isSneaking)
        println("a")
    }



    @SideOnly(Side.CLIENT)
    @SubscribeEvent
    @JvmStatic
    fun onOverlay(worldRenderEvent: RenderGameOverlayEvent) {
        if(Minecraft().currentScreen == null) {
            PacketHandler.NETWORK.sendToServer(PacketRequestUpdate())
            renderDuelBar()
            if(worldRenderEvent.type == RenderGameOverlayEvent.ElementType.HEALTH || worldRenderEvent.type == RenderGameOverlayEvent.ElementType.FOOD)
                worldRenderEvent.isCanceled = true
        }

        if(worldRenderEvent.type == RenderGameOverlayEvent.ElementType.HOTBAR /*&& !Minecraft().player.isCreative*/)
            renderHpBar()
    }


    private val hpBar_StartSprite = Sprite(ResourceLocation(SAO.MOD_ID,"textures/ui/hpbar_start.png"))
    private val hpBar_NameSprite = Sprite(ResourceLocation(SAO.MOD_ID,"textures/ui/hpbar_name.png"))
    private val hpBarBackgroundSprite = Sprite(ResourceLocation(SAO.MOD_ID,"textures/ui/hpbarempty.png"))
    val spriteFull4Size = Sprite(ResourceLocation("sao:textures/ui/hpbarfull.png"))

    @SideOnly(Side.CLIENT)
    private fun renderHpBar() {

        val mc = Minecraft()
        val fontRenderer = mc.fontRenderer
        val wbar = 225f
        val hbar = 19.8f

        var offSetY = 3f
        var offSetX = 0f

        if (mc.gameSettings.showDebugInfo) {
            offSetY += 150f
        }

        GlStateManager.pushMatrix()
        GlStateManager.enableAlpha()
        GlStateManager.enableBlend()
        hpBar_StartSprite.bind()
        hpBar_StartSprite.draw(0, 0f, offSetY, 10f, hbar)

        hpBar_NameSprite.bind()
        val playerName =  mc.player.displayName.formattedText
        val playerNameOffsetX = fontRenderer.getStringWidth(playerName) + 11f
        offSetX+=9
        hpBar_NameSprite.draw(0, offSetX, offSetY, playerNameOffsetX, hbar)

        hpBarBackgroundSprite.bind()
        val hpPercent = mc.player.health / mc.player.maxHealth
        val hpBar = (200.0f * hpPercent).toInt()

        offSetX+= playerNameOffsetX
        hpBarBackgroundSprite.draw(0, offSetX, offSetY, wbar, 26.2f)
        GlStateManager.popMatrix()

        fontRenderer.drawString(playerName, 14f, offSetY + 6, Color.WHITE.rgb, true)
        fontRenderer.drawString(mc.player.health.roundToInt().toString(), offSetX + 130, offSetY + 17, Color.WHITE.rgb, true)
        fontRenderer.drawString(mc.player.maxHealth.roundToInt().toString(), offSetX + 155, offSetY + 17, Color.WHITE.rgb, true)
        fontRenderer.drawString(mc.player.experienceLevel.toString(), offSetX + 200, offSetY + 16.5f, Color.WHITE.rgb, true)

        val width = spriteFull4Size.width
        val a = wbar*0f/width // length of texture 2 until bar
        val b = Minecraft().displayWidth / 6.55f //wbar*1190f/width // length of bar
        val c = Minecraft().displayWidth / 28.5f // length of texture 1 until bar
        val d = Minecraft().displayHeight / 170f // height of texture 1 until bar
        val wTotal = a + b * Minecraft().player.health / Minecraft().player.maxHealth

        val spriteFull = Sprite(ResourceLocation("sao:textures/ui/hpbarfull.png"), 0, 0) // width, height is where we start drawing the texture from (the coords of the image)
        spriteFull.bind()
        spriteFull.draw(0, offSetX + 1f, offSetY + 4, hpBar + 10f, 11f)

        /*
        hpBarBackgroundSprite.bind()
        val hbar = Minecraft().displayHeight / 25f
        val wbar = Minecraft().displayWidth / 5f
        GlStateManager.enableAlpha()

        GlStateManager.color(1f, 1f, 1f)
        hpBarBackgroundSprite.draw(0, 0f, 0f, wbar, hbar)
        val width = spriteFull4Size.width
        val a = wbar*0f/width // length of texture 2 until bar
        val b = Minecraft().displayWidth / 6.55f //wbar*1190f/width // length of bar
        val c = Minecraft().displayWidth / 28.5f // length of texture 1 until bar
        val d = Minecraft().displayHeight / 170f // height of texture 1 until bar
        val wTotal = a + b * Minecraft().player.health / Minecraft().player.maxHealth

        val spriteFull = Sprite(ResourceLocation("sao:textures/ui/$hpBarFull.png"), wTotal.toInt(), 0)//57/99 * hpBarBackgroundSprite.height)
        *//*val clipper = LayerClippingHandler()
        clipper.clippingSprite = spriteFull4Size
        clipper*//*
        spriteFull.bind()
        spriteFull.draw(0, c, d, wTotal, hbar*46/99)
        renderer.drawString(Minecraft().player.health.roundToInt().toString(), Minecraft().displayWidth / 7.8f, Minecraft().displayHeight / 35f, Color.WHITE.rgb, false)
        renderer.drawString(Minecraft().player.maxHealth.roundToInt().toString(), Minecraft().displayWidth / 6.6f, Minecraft().displayHeight / 35f, Color.WHITE.rgb, false)
        */
    }

    const val duelBlank = "duel_blank"
    const val duel7550 = "duel7550"
    const val duel3 = "duel3"
    val spriteDuels = Sprite(ResourceLocation("sao:textures/ui/$duel3.png"))

    @SideOnly(Side.CLIENT)
    private fun renderDuelBar() {
        val dueling = Utils.getPlayerData(Minecraft().player.uniqueID)?.dueling ?: return //Minecraft().world.playerEntities.firstOrNull { it != Minecraft().player }?.uniqueID ?: return
        spriteDuels.bind()

        val wrect = 0.666f* 1.5f*Math.min(Minecraft().displayWidth / 3f, 256f) // haha, wrect
        val hrect = 0.666f* Minecraft().displayHeight / 16f
        val xbox = 9*Minecraft().displayWidth / 50+Minecraft().displayWidth/30 // haha, xbox.
        val xrect = 4f*Minecraft().displayWidth / 24f+Minecraft().displayWidth/30
        val xtext = xbox+Minecraft().displayWidth/50

        val yOffset = Minecraft().displayHeight/64f
        GlStateManager.enableAlpha()

        spriteDuels.draw(0, xrect, yOffset, wrect, hrect)
        /*val duelBlank = "duel_blank"
        val duel7550 = "duel7550"
        Minecraft.getMinecraft().textureManager.bindTexture(ResourceLocation("sao:textures/ui/${duel7550}.png"))
        GlStateManager.enableTexture2D()
        Minecraft().ingameGUI.drawTexturedModalRect(
                2.25.toFloat()*Minecraft().displayWidth / 12.toFloat(), 0.toFloat(),
                0, 0,
                Math.min(Minecraft().displayWidth / 3, 256), Minecraft().displayHeight / 16)
        */

        PlayerHeadIcon(dueling).draw(xbox, Minecraft().displayHeight / 128 + yOffset.toInt(), Minecraft().displayHeight / 32, Minecraft().displayHeight / 32)

        PlayerHeadIcon(Minecraft().player.uniqueID).draw((2*xrect + wrect - xbox).toInt() - Minecraft().displayHeight / 32, Minecraft().displayHeight / 128 + yOffset.toInt(), Minecraft().displayHeight / 32, Minecraft().displayHeight / 32)

        Minecraft().fontRenderer.drawString(Minecraft().world.playerEntities.first { it.uniqueID == dueling }.name, xtext, 3 * Minecraft().displayHeight / 128 + yOffset.toInt(), Color.BLACK.rgb)

        val wtext = Minecraft().fontRenderer.getStringWidth(Minecraft().player.name)
        Minecraft().fontRenderer.drawString(Minecraft().player.name, (2 * xrect + wrect - xtext - wtext).toInt(), 3 * Minecraft().displayHeight / 128 + yOffset.toInt(), Color.BLACK.rgb)
    }
}
