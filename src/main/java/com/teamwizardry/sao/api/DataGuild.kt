package com.teamwizardry.sao.api

import com.teamwizardry.librarianlib.features.base.block.tile.module.ModuleInventory
import com.teamwizardry.sao.api.capability.world.SAOWorldCap
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.nbt.NBTTagList
import net.minecraft.nbt.NBTTagString
import net.minecraft.world.World
import net.minecraftforge.common.util.Constants
import net.minecraftforge.common.util.INBTSerializable
import java.util.UUID
import kotlin.collections.HashSet

/**
 * SERVER SIDED MANIPULATION ONLY.
 */
class DataGuild(var leader: UUID = UUID.randomUUID()) : INBTSerializable<NBTTagCompound> {

    var guildUUID = UUID.randomUUID()

    var name: String? = null

    val members = HashSet<UUID>()

    val invites = HashSet<UUID>()

    var storage = ModuleInventory(54)

    companion object {
        fun createGuild(compound: NBTTagCompound): DataGuild {
            val guild = DataGuild()
            guild.deserializeNBT(compound)
            return guild
        }
    }

    init {
        members.add(leader)
    }

    fun disbandParty() {
        members.clear()
    }

    fun addGuildMember(world: World, uuid: UUID): Boolean {
        if (members.contains(uuid)) return false

        SAOWorldCap.get(world).getGuildContainingMember(uuid)?.removeGuildMember(uuid)

        return members.add(uuid)
    }

    fun removeGuildMember(uuid: UUID): Boolean {
        return if (uuid == leader) {
            disbandParty()
            true
        } else members.remove(uuid)
    }

    fun isLeader(uuid: UUID): Boolean {
        return uuid == leader
    }

    fun isLonely(): Boolean {
        return members.size <= 1
    }

    override fun serializeNBT(): NBTTagCompound {
        val nbt = NBTTagCompound()

        name?.let {
            nbt.setString("name", it)
        }

        nbt.setString("leader", leader.toString())

        val nbtMembers = NBTTagList()
        for (member in members) {
            nbtMembers.appendTag(NBTTagString(member.toString()))
        }
        nbt.setTag("members", nbtMembers)

        val nbtInvites = NBTTagList()
        for (invite in invites) {
            nbtInvites.appendTag(NBTTagString(invite.toString()))
        }
        nbt.setTag("invites", nbtInvites)

        nbt.setString("guild_uuid", guildUUID.toString())

        nbt.setTag("storage", storage.writeToNBT(true))

        return nbt
    }

    override fun deserializeNBT(nbt: NBTTagCompound) {

        name = if (nbt.hasKey("name")) {
            nbt.getString("name")
        } else null

        if (nbt.hasKey("leader")) {
            leader = UUID.fromString(nbt.getString("leader"))
        }

        members.clear()
        if (nbt.hasKey("members")) {
            val list4 = nbt.getTagList("members", Constants.NBT.TAG_STRING)
            for (base in list4) {
                if (base is NBTTagString) {
                    UUID.fromString(base.string)?.let { members.add(it) }
                }
            }
        }
        members.add(leader)

        invites.clear()
        if (nbt.hasKey("invites")) {
            val list4 = nbt.getTagList("invites", Constants.NBT.TAG_STRING)
            for (base in list4) {
                if (base is NBTTagString) {
                    UUID.fromString(base.string)?.let { invites.add(it) }
                }
            }
        }
        invites.add(leader)

        if (nbt.hasKey("guild_uuid")) {
            guildUUID = UUID.fromString(nbt.getString("guild_uuid"))
        }

        if (nbt.hasKey("storage")) {
            storage= ModuleInventory(54)
            storage.readFromNBT(nbt.getCompoundTag("storage"))
        }

    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as DataGuild

        if (guildUUID != other.guildUUID) return false

        return true
    }

    override fun hashCode(): Int {
        return guildUUID?.hashCode() ?: 0
    }
}
