package com.teamwizardry.sao.api

import com.teamwizardry.sao.SAO
import com.teamwizardry.sao.api.capability.world.SAOWorldCap
import com.teamwizardry.sao.api.util.Utils
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.nbt.NBTTagList
import net.minecraft.nbt.NBTTagString
import net.minecraft.world.World
import net.minecraftforge.common.util.Constants
import net.minecraftforge.common.util.INBTSerializable
import java.util.Objects
import java.util.UUID
import kotlin.collections.HashSet

class DataFriends(val holderData: SAOPlayerData) : INBTSerializable<NBTTagCompound> {

    val listFriends = HashSet<SAOPlayerData>()
    val invites = HashSet<SAOPlayerData>()

    fun syncBetweenFriend(world: World, friendPlayerData: SAOPlayerData) {
     //   if (world.isRemote) {
     //       PacketHandler.NETWORK.sendToServer(PacketUpdateFriendsClient(friendPlayerData.friends.serializeNBT(), friendPlayerData.uuid))
     //       PacketHandler.NETWORK.sendToServer(PacketUpdateFriendsClient(serializeNBT(), holderData.uuid))
     //   } else {
     //       syncToHolder(world)

     //       FMLCommonHandler.instance().minecraftServerInstance.playerList.getPlayerByUUID(friendPlayerData.uuid).let {
     //           PacketHandler.NETWORK.sendTo(PacketUpdateFriends(friendPlayerData.friends.serializeNBT()), it)
     //       }
     //   }
    }

    fun syncToHolder(world: World) {
    //    if (world.isRemote) {
    //        PacketHandler.NETWORK.sendToServer(PacketUpdateFriendsClient(serializeNBT(), holderData.uuid))
    //    } else {
    //        FMLCommonHandler.instance().minecraftServerInstance.playerList.getPlayerByUUID(holderData.uuid).let {
    //            PacketHandler.NETWORK.sendTo(PacketUpdateFriends(serializeNBT()), it)
    //        }
    //    }
    }

    fun addFriend(world: World, friendPlayerData: SAOPlayerData): Boolean {
        return if (listFriends.add(friendPlayerData)) {
            syncBetweenFriend(world, friendPlayerData)
            true
        } else false
    }

    fun removeFriend(world: World, friendPlayerData: SAOPlayerData): Boolean {
        val res1 = friendPlayerData.friends.listFriends.remove(this.holderData)
        val res2 = listFriends.remove(friendPlayerData)
        return if (res1 || res2) {
            syncBetweenFriend(world, friendPlayerData)
            true
        } else false
    }

    fun removeInvite(world: World, friendPlayerData: SAOPlayerData): Boolean {
        return if (invites.remove(friendPlayerData)) {
            syncToHolder(world)
            true
        } else false
    }

    fun addInvite(world: World, friendPlayerData: SAOPlayerData): Boolean {
        return if (invites.add(friendPlayerData)) {
            syncToHolder(world)
            true
        } else false
    }

    fun acceptInvite(world: World, friendPlayerData: SAOPlayerData): Boolean {
        return if (invites.contains(friendPlayerData)) {
            invites.remove(friendPlayerData) && addFriend(world, friendPlayerData)
        } else false
    }

    override fun serializeNBT(): NBTTagCompound {
        val nbt = NBTTagCompound()

        val nbtFriends = NBTTagList()
        for (member in listFriends) {
            nbtFriends.appendTag(NBTTagString(member.uuid.toString()))
        }
        nbt.setTag("friends", nbtFriends)

        val nbtInvites = NBTTagList()
        for (member in invites) {
            nbtInvites.appendTag(NBTTagString(member.uuid.toString()))
        }
        nbt.setTag("invites", nbtInvites)

        return nbt
    }

    override fun deserializeNBT(nbt: NBTTagCompound) {

        listFriends.clear()
        if (nbt.hasKey("friends")) {
            val list = nbt.getTagList("friends", Constants.NBT.TAG_STRING)
            for (i in 0 until list.tagCount()) {
                val uuid = UUID.fromString(list.getStringTagAt(i))
                Utils.getPlayerData(uuid)?.let {
                    listFriends.add(it)
                }
            }
        }

        invites.clear()
        if (nbt.hasKey("invites")) {
            val list = nbt.getTagList("invites", Constants.NBT.TAG_STRING)
            for (i in 0 until list.tagCount()) {
                val uuid = UUID.fromString(list.getStringTagAt(i))
                Utils.getPlayerData(uuid)?.let {
                    invites.add(it)
                }
            }
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val party = other as DataFriends?
        return this.holderData.uuid == party!!.holderData.uuid
    }

    override fun hashCode(): Int {
        return Objects.hash(this.holderData.uuid)
    }

    companion object {

        fun create(world: World, uuid: UUID, compound: NBTTagCompound): DataFriends? {
            val cap = SAOWorldCap.get(SAO.proxy.getWorld())
            cap.getSAOPlayerData(uuid)?.let {
                val friends = DataFriends(it)
                friends.deserializeNBT(compound)
                return friends
            }
            return null
        }
    }
}
