package com.teamwizardry.sao.api

import com.teamwizardry.sao.common.command.CommandDuelRequest
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.nbt.NBTTagList
import net.minecraft.nbt.NBTTagString
import net.minecraft.util.math.BlockPos
import net.minecraft.world.World
import net.minecraftforge.common.util.Constants
import net.minecraftforge.common.util.INBTSerializable
import java.util.*

class SAOPlayerData(val uuid: UUID, var name: String = "<NULL>") : INBTSerializable<NBTTagCompound> {

    val partyInvites = HashSet<UUID>()

    val party: DataParty = DataParty(this)

    val friends: DataFriends = DataFriends(this)

    var proposeTo: UUID? = null
    var marriedTo: UUID? = null

    var currentlyTradingWith: String? = null

    var challenged: UUID? = null
    var challengedMode: CommandDuelRequest.DuelType? = null
    var dueling: UUID? = null
    var duelingMode: CommandDuelRequest.DuelType? = null
    var posDuel: BlockPos? = null

    companion object {
        fun create(uuid: UUID, compound: NBTTagCompound): SAOPlayerData {
            val data = SAOPlayerData(uuid)
            data.deserializeNBT(compound)
            return data
        }

        private fun syncToPlayer(world: World, uuid: UUID, newData: SAOPlayerData) {
          //  if (world.isRemote) {
          //        PacketHandler.NETWORK.sendToServer(PacketUpdateDataClient(newData.serializeNBT(), uuid))
          //  } else {
          //      world.getPlayerEntityByUUID(uuid)?.let {
          //          PacketHandler.NETWORK.sendTo(PacketUpdateData(newData.serializeNBT()), it as EntityPlayerMP?)
          //      }
          //  }
        }
    }

    fun acceptPartyInvite(world: World, party: DataParty): Boolean {
        return if (partyInvites.contains(party.getLeader().uuid) && partyInvites.remove(party.getLeader().uuid) && party.addPartyMember(world, this)) {
            syncToPlayer(world, uuid, this)
            true
        } else false
    }

    override fun serializeNBT(): NBTTagCompound {
        val tag = NBTTagCompound()

        tag.setTag("party", party.serializeNBT())
        tag.setTag("list_friends", friends.serializeNBT())

        val list = NBTTagList()
        for (party in partyInvites) {
            list.appendTag(NBTTagString(party.toString()))
        }
        tag.setTag("party_invites", list)

        tag.setString("player_name", name)

        if (proposeTo != null) {
            tag.setString("proposeTo", proposeTo.toString())
        }

        if (marriedTo != null) {
            tag.setString("marriedTo", marriedTo.toString())
        }

        if(currentlyTradingWith != null) tag.setString("current_trading_partner", currentlyTradingWith)

        if(challenged != null) {
            tag.setString("challenged", challenged.toString())
        }

        if(dueling != null) {
            tag.setString("challenged", challenged.toString())
        }

        if(challengedMode != null) {
            tag.setInteger("mode", challengedMode!!.ordinal)
        }

        if(duelingMode != null) {
            tag.setInteger("modeDueling", duelingMode!!.ordinal)
        }

        if(posDuel != null) {
            tag.setLong("posDuel", posDuel!!.toLong())
        }

        return tag
    }

    override fun deserializeNBT(nbt: NBTTagCompound) {
        partyInvites.clear()
        if (nbt.hasKey("party_invites")) {
            val list2 = nbt.getTagList("party_invites", Constants.NBT.TAG_STRING)
            for (cmp in list2) {
                if (cmp is NBTTagString)
                    partyInvites.add(UUID.fromString(cmp.string))
            }
        }

        party.deserializeNBT(nbt.getCompoundTag("party"))
        friends.deserializeNBT(nbt.getCompoundTag("list_friends"))

        name = if (nbt.hasKey("player_name")) nbt.getString("player_name") ?: "<NULL>"
        else "<NULL>"

        if (nbt.hasKey("proposeTo")) {
            proposeTo = UUID.fromString(nbt.getString("proposeTo"))
        }

        if (nbt.hasKey("marriedTo")) {
            marriedTo = UUID.fromString(nbt.getString("marriedTo"))
        }

        if(nbt.hasKey("current_trading_partner")) currentlyTradingWith = nbt.getString("current_trading_partner")

        if(nbt.hasKey("challenged") && nbt.getString("challenged") != null) {
            challenged = UUID.fromString(nbt.getString("challenged"))
        }

        if(nbt.hasKey("dueling")) {
            dueling = UUID.fromString(nbt.getString("dueling"))
        }

        if(nbt.hasKey("mode")) {
            challengedMode = CommandDuelRequest.DuelType.values()[nbt.getInteger("mode")]
        }

        if(nbt.hasKey("modeDueling")) {
            duelingMode = CommandDuelRequest.DuelType.values()[nbt.getInteger("modeDueling")]
        }

        if(nbt.hasKey("posDuel")) {
            posDuel = BlockPos.fromLong(nbt.getLong("posDuel"))
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SAOPlayerData

        if (uuid != other.uuid) return false

        return true
    }

    override fun hashCode(): Int {
        return uuid.hashCode()
    }


}
