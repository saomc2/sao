package com.teamwizardry.sao.api.util

import com.teamwizardry.sao.SAO
import com.teamwizardry.sao.api.SAOPlayerData
import com.teamwizardry.sao.api.capability.world.SAOWorldCap
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.util.text.ITextComponent
import net.minecraft.util.text.Style
import net.minecraft.util.text.TextComponentTranslation
import net.minecraft.util.text.TextFormatting
import org.jetbrains.annotations.Nullable
import java.util.*

class Utils {

    companion object {
        @Nullable
        fun getPlayerData(uuid: UUID): SAOPlayerData? {
            val world = SAO.proxy.getWorld() ?: return null
            return SAOWorldCap.get(world).getSAOPlayerData(uuid)
        }

        fun sendMessage(player: EntityPlayer, messageKey: String, formatting: TextFormatting = TextFormatting.YELLOW, vararg placeHolders: ITextComponent) {
            player.sendMessage(TextComponentTranslation(messageKey, player.displayName, *placeHolders).setStyle(Style().setColor(formatting)))
        }
    }
}
