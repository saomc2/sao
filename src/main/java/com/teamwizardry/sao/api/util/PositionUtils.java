package com.teamwizardry.sao.api.util;

import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.ArrayList;
import java.util.List;

public class PositionUtils {

    /**
     * Copied basically one to one from EntityDoppleganger in the Botania code,
     * with accordance with the Botania license.
     */
    public static boolean hasProperArena(World world, BlockPos startPos, int range) {
        List<BlockPos> trippedPositions = new ArrayList<>();
        boolean tripped = false;

        int heightCheck = 3;
        int heightMin = 2;
        for(int i = -range; i < range + 1; i++)
            for(int j = -range; j < range + 1; j++) {
                if(Math.abs(i) == 4 && Math.abs(j) == 4 || Math.hypot(i, j) > range)
                    continue; // Ignore pylons and out of circle

                int air = 0;

                yCheck: {
                    BlockPos pos = null;
                    int trippedColumn = 0;

                    for(int k = heightCheck + heightMin; k >= -heightCheck; k--) {
                        pos = startPos.add(i, k, j);
                        boolean isAir = world.getBlockState(pos).getCollisionBoundingBox(world, pos) == null;
                        if(isAir)
                            air++;
                        else {
                            if(air >= 2)
                                break yCheck;
                            else if(trippedColumn < 2) {
                                trippedPositions.add(pos);
                                trippedColumn++;
                            }
                            air = 0;
                        }
                    }

                    if(trippedColumn == 0)
                        trippedPositions.add(pos);

                    tripped = true;
                }
            }

        return !tripped;
    }
}
