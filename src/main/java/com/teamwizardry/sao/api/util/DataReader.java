package com.teamwizardry.sao.api.util;

import com.google.gson.*;
import com.google.gson.internal.Streams;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;

import javax.annotation.Nullable;
import javax.imageio.ImageIO;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.Proxy;
import java.net.URL;
import java.nio.charset.StandardCharsets;

/**
 * @author LatvianModder
 * Stolen with permission from FTBLib
 */
public abstract class DataReader {

    public static DataReader get(URL url, String contentType, Proxy proxy) {
        return new DataReader() {
            private HttpURLConnection getConnection() throws Exception
            {
                HttpURLConnection connection = (HttpURLConnection) url.openConnection(proxy);

                if (connection instanceof HttpsURLConnection)
                {
                    TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager()
                    {
                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers()
                        {
                            return null;
                        }

                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType)
                        {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType)
                        {
                        }
                    }};

                    try
                    {
                        SSLContext sc = SSLContext.getInstance("SSL");
                        sc.init(null, trustAllCerts, new java.security.SecureRandom());
                        ((HttpsURLConnection) connection).setSSLSocketFactory(sc.getSocketFactory());
                    }
                    catch (Exception e)
                    {
                    }
                }

                connection.setRequestMethod("GET");
                connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3");

                if (!contentType.isEmpty())
                {
                    connection.setRequestProperty("Content-Type", contentType);
                }

                connection.setDoInput(true);

                int responseCode = connection.getResponseCode();

                if (responseCode / 100 != 2)
                {
                    throw new Exception(responseCode + "");
                }

                return connection;
            }

            @Override
            public String string(int bufferSize) throws Exception
            {
                HttpURLConnection connection = getConnection();

                try (InputStream stream = connection.getInputStream())
                {
                    return readStringFromStream(stream, bufferSize);
                }
                finally
                {
                    connection.disconnect();
                }
            }

            @Override
            public JsonElement json() throws Exception
            {
                HttpURLConnection connection = getConnection();

                try (InputStream stream = connection.getInputStream())
                {

                    JsonReader jsonReader = new JsonReader(new InputStreamReader(stream, StandardCharsets.UTF_8));
                    JsonElement element;
                    jsonReader.setLenient(true);
                    element = Streams.parse(jsonReader);

                    if (!element.isJsonNull() && jsonReader.peek() != JsonToken.END_DOCUMENT) {
                        throw new JsonSyntaxException("Did not consume the entire document.");
                    }

                    return element;
                }
                finally
                {
                    connection.disconnect();
                }
            }

            @Override
            public BufferedImage image() throws Exception
            {
                HttpURLConnection connection = getConnection();

                try (InputStream stream = connection.getInputStream())
                {
                    return ImageIO.read(stream);
                }
                finally
                {
                    connection.disconnect();
                }
            }
        };
        //new HttpDataReader(url, RequestMethod.GET, contentType, null, proxy);
    }

    public static DataReader get(@Nullable String string) {
        return new DataReader() {
            @Override
            public String string(int bufferSize) throws Exception {
                return string;
            }

            @Override
            public JsonElement json() throws Exception {
                if (string.equals("null")) {
                    return JsonNull.INSTANCE;
                } else if (string.equals("true")) {
                    return new JsonPrimitive(true);
                } else if (string.equals("false")) {
                    return new JsonPrimitive(false);
                } else if (string.length() == 2) {
                    char c0 = string.charAt(0);
                    char c1 = string.charAt(1);

                    if (c0 == '[' && c1 == ']') {
                        return new JsonArray();
                    } else if (c0 == '{' && c1 == '}') {
                        return new JsonObject();
                    } else if (c0 == '"' && c1 == '"') {
                        return new JsonPrimitive("");
                    }
                }

                try (Reader reader = new StringReader(string)) {

                    JsonReader jsonReader = new JsonReader(reader);
                    JsonElement element;
                    jsonReader.setLenient(true);
                    element = Streams.parse(jsonReader);

                    if (!element.isJsonNull() && jsonReader.peek() != JsonToken.END_DOCUMENT) {
                        throw new JsonSyntaxException("Did not consume the entire document.");
                    }

                    return element;
                }
            }

            @Override
            public BufferedImage image() {
                throw new IllegalStateException("Can't parse string as image!");
            }
        };
    }

    public abstract String string(int bufferSize) throws Exception;

    public abstract JsonElement json() throws Exception;

    public abstract BufferedImage image() throws Exception;

    protected static String readStringFromStream(InputStream stream, int bufferSize) throws Exception
    {
        try (Reader in = new InputStreamReader(stream, StandardCharsets.UTF_8))
        {
            char[] buffer = new char[bufferSize];
            StringBuilder out = new StringBuilder();
            int read;
            do
            {
                read = in.read(buffer, 0, bufferSize);
                if (read > 0)
                {
                    out.append(buffer, 0, read);
                }
            }
            while (read >= 0);
            return out.toString();
        }
    }
}