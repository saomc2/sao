package com.teamwizardry.sao.api

import com.teamwizardry.sao.api.util.Utils
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.nbt.NBTTagList
import net.minecraft.nbt.NBTTagString
import net.minecraft.world.World
import net.minecraftforge.common.util.Constants
import net.minecraftforge.common.util.INBTSerializable
import java.util.Objects
import java.util.UUID
import kotlin.collections.HashSet

/**
 * SERVER SIDED MANIPULATION ONLY.
 */
class DataParty(val holderData: SAOPlayerData) : INBTSerializable<NBTTagCompound> {

    private var leaderData: SAOPlayerData
    var name: String? = null

    val members = HashSet<SAOPlayerData>()

    companion object {
        fun createParty(holderData: SAOPlayerData, compound: NBTTagCompound): DataParty {
            val party = DataParty(holderData)
            party.deserializeNBT(compound)
            return party
        }

        private fun syncToMembers(world: World, members: HashSet<SAOPlayerData>, party: DataParty) {
            for (member in members) {
                syncToMember(world, member, party)
            }
        }

        private fun syncToMember(world: World, memberData: SAOPlayerData, party: DataParty) {
            memberData.party.deserializeNBT(party.serializeNBT())
         //   if (world.isRemote) {
         //       PacketHandler.NETWORK.sendToServer(PacketUpdatePartyClient(party, memberData.uuid))
         //   } else {
         //       FMLCommonHandler.instance().minecraftServerInstance.playerList.getPlayerByUUID(memberData.uuid).let {
         //           PacketHandler.NETWORK.sendTo(PacketUpdateParty(party), it)
         //       }
         //   }
        }
    }

    init {
        this.leaderData = holderData
        members.add(holderData)
    }

    fun disbandParty(world: World, sync: Boolean = true) {
        val oldMembers = HashSet<SAOPlayerData>(this.members)
        members.clear()
        if (sync)
            syncToMembers(world, oldMembers, this)
    }

    fun addPartyMember(world: World, playerData: SAOPlayerData): Boolean {
        if (!playerData.party.isLonely()) {
            playerData.party.removePartyMember(world, playerData)
        }

        return if (members.add(playerData)) {
            syncToMembers(world, members, playerData.party)
            true
        } else false
    }

    fun removePartyMember(world: World, playerData: SAOPlayerData): Boolean {
        return when {
            isLonely() -> false
            playerData == leaderData -> {
                disbandParty(world)
                true
            }
            else -> return if (members.remove(playerData)) {
                syncToMembers(world, members, this)

                playerData.party.disbandParty(world, false)
                playerData.party.setLeader(world, playerData)
                true
            } else false
        }
    }

    fun getLeader(): SAOPlayerData {
        return this.leaderData
    }

    fun setLeader(world: World, newLeader: SAOPlayerData) {
        leaderData = newLeader
        members.add(newLeader)
        syncToMembers(world, members, this)
    }

    fun isLeader(playerData: SAOPlayerData): Boolean {
        return playerData == leaderData
    }

    fun isLonely(): Boolean {
        return members.size <= 1
    }

    override fun serializeNBT(): NBTTagCompound {
        val nbt = NBTTagCompound()

        name?.let {
            nbt.setString("name", it)
        }

        nbt.setString("leader_data", leaderData.uuid.toString())

        val nbtMembers = NBTTagList()
        for (member in members) {
            nbtMembers.appendTag(NBTTagString(member.uuid.toString()))
        }
        nbt.setTag("members", nbtMembers)

        return nbt
    }

    override fun deserializeNBT(nbt: NBTTagCompound) {

        name = if (nbt.hasKey("name")) {
            nbt.getString("name")
        } else null

        if (nbt.hasKey("leader_data")) {
            leaderData = Utils.getPlayerData(UUID.fromString(nbt.getString("leader_data"))) ?: holderData
        }

        members.clear()
        if (nbt.hasKey("members")) {
            val list4 = nbt.getTagList("members", Constants.NBT.TAG_STRING)
            for (base in list4) {
                if (base is NBTTagString) {
                    Utils.getPlayerData(UUID.fromString(base.string))?.let { members.add(it) }
                }
            }
        }
        members.add(leaderData)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val party = other as DataParty?
        return this.leaderData.uuid == party!!.leaderData.uuid
    }

    override fun hashCode(): Int {
        return Objects.hash(this.leaderData.uuid)
    }
}
