package com.teamwizardry.sao.api.capability.world;

import com.teamwizardry.sao.SAO;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import java.util.Objects;

@Mod.EventBusSubscriber(modid = SAO.MOD_ID)
public final class SAOWorldCap {
	private static final ResourceLocation SAO_WORLD_LOC = new ResourceLocation(SAO.MOD_ID, "sao_world");
	@CapabilityInject(ISAOWorld.class)
	private static final Capability<ISAOWorld> CAPABILITY = null;

	private SAOWorldCap() {
	}

	public static Capability<ISAOWorld> capability() {
		//noinspection ConstantConditions
		return Objects.requireNonNull(CAPABILITY, "CAPABILITY");
	}

	// call in preinit
	public static void init() {
		CapabilityManager.INSTANCE.register(ISAOWorld.class, new WizardryWorldStorage(), StandardSAOWorld.Companion::create);
	}

	public static ISAOWorld get(World world) {
		ISAOWorld cap = world.getCapability(capability(), null);
		if (cap == null) {
			throw new IllegalStateException("Missing capability: " + world.getWorldInfo().getWorldName() + "/" + world.provider.getDimensionType().getName());
		}
		return cap;
	}

	@SubscribeEvent
	public static void onAttachCapabilities(AttachCapabilitiesEvent<World> event) {
		event.addCapability(SAO_WORLD_LOC, StandardSAOWorld.Companion.create(event.getObject()));
	}

	// other event subscriptions related to cap behavior

	private static final class WizardryWorldStorage implements Capability.IStorage<ISAOWorld> {
		@Override
		public NBTBase writeNBT(Capability<ISAOWorld> capability, ISAOWorld world, EnumFacing side) {
			return world.serializeNBT();
		}

		@Override
		public void readNBT(Capability<ISAOWorld> capability, ISAOWorld world, EnumFacing side, NBTBase nbt) {
			if (nbt instanceof NBTTagCompound) {
				world.deserializeNBT((NBTTagCompound) nbt);
			}
		}
	}
}
