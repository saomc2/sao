package com.teamwizardry.sao.api.capability.player;

import net.minecraft.entity.Entity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;

/**
 * Created by Demoniaque on 8/16/2016.
 */
public interface ISAOPlayer extends ICapabilitySerializable<NBTTagCompound> {

	ItemStack[] getFavoriteItems();

	void setFavoriteItemSlot(int index, ItemStack stack);

	void dataChanged(Entity player);
}
