package com.teamwizardry.sao.api.capability.player;

import com.teamwizardry.librarianlib.features.network.PacketHandler;
import com.teamwizardry.sao.common.network.PacketUpdateCaps;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.Constants;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Created by Demoniaque on 8/16/2016.
 */
public class DefaultSAOPlayer implements ISAOPlayer {

	private ItemStack[] favoriteItems = new ItemStack[9];

	private EntityPlayer player;

	public DefaultSAOPlayer() {
	}

	public DefaultSAOPlayer(EntityPlayer player) {
		this.player = player;
		for (int i = 0; i < favoriteItems.length; i++) {
			favoriteItems[i] = ItemStack.EMPTY;
		}
	}

	@Override
	public ItemStack[] getFavoriteItems() {
		return favoriteItems;
	}

	@Override
	public void setFavoriteItemSlot(int index, ItemStack stack) {
		favoriteItems[index] = stack;
	}

	@Override
	public NBTTagCompound serializeNBT() {
		NBTTagCompound tag = new NBTTagCompound();
		NBTTagList list = new NBTTagList();
		for (ItemStack stack : getFavoriteItems()) {
			list.appendTag(stack.serializeNBT());
		}
		tag.setTag("favorite_items", list);

		return tag;
	}

	@Override
	public void deserializeNBT(NBTTagCompound nbt) {

		if (nbt.hasKey("favorite_items")) {
			NBTTagList list = nbt.getTagList("favorite_items", Constants.NBT.TAG_COMPOUND);
			for (int i = 0; i < list.tagCount(); i++) {
				NBTTagCompound stackNBT = list.getCompoundTagAt(i);
				ItemStack stack = new ItemStack(stackNBT);
				setFavoriteItemSlot(i, stack);
			}
		}
	}

	@Override
	public void dataChanged(Entity entity) {
		if (entity instanceof EntityPlayer && !entity.getEntityWorld().isRemote)
			PacketHandler.NETWORK.sendTo(new PacketUpdateCaps(serializeNBT()), (EntityPlayerMP) entity);
	}

	@Override
	public boolean hasCapability(@Nonnull Capability<?> capability, @Nullable EnumFacing facing) {
		return capability == SAOPlayerCap.capability();
	}

	@Nullable
	@Override
	public <T> T getCapability(@Nonnull Capability<T> capability, @Nullable EnumFacing facing) {
		return capability == SAOPlayerCap.capability() ? SAOPlayerCap.capability().cast(this) : null;
	}
}
