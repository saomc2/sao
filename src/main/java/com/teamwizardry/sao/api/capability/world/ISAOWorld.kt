package com.teamwizardry.sao.api.capability.world

import com.teamwizardry.sao.api.DataGuild
import com.teamwizardry.sao.api.SAOPlayerData
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.nbt.NBTTagCompound
import net.minecraftforge.common.capabilities.ICapabilitySerializable
import java.util.*

interface ISAOWorld : ICapabilitySerializable<NBTTagCompound> {

    fun getAllPlayerData(): HashMap<UUID, SAOPlayerData>

    fun addSAOPlayerData(player: EntityPlayer): SAOPlayerData?

    fun getSAOPlayerData(uuid: UUID): SAOPlayerData?

    fun getGuildContainingMember(uuid: UUID): DataGuild?

    fun addNewGuild(guild: DataGuild): Boolean

    fun removeGuild(guild: DataGuild): Boolean

    fun getGuildsContainingInviteFor(uuid: UUID): HashSet<DataGuild>

    fun getGuildFromUUID(uuid: UUID): DataGuild?

    fun getCurrentTradingPartner(uuid: UUID): String?
}
