package com.teamwizardry.sao.api.capability.world

import com.teamwizardry.sao.api.DataGuild
import com.teamwizardry.sao.api.SAOPlayerData
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.nbt.NBTTagList
import net.minecraft.util.EnumFacing
import net.minecraft.world.World
import net.minecraftforge.common.capabilities.Capability
import net.minecraftforge.common.util.Constants
import java.util.*
import kotlin.collections.HashSet

class StandardSAOWorld : ISAOWorld {

    private var world: World? = null

    private val playerData = HashMap<UUID, SAOPlayerData>()
    private val guilds = HashSet<DataGuild>()

    override fun getGuildFromUUID(uuid: UUID): DataGuild? {
        for (guild in guilds) {
            if (guild.guildUUID == uuid) return guild
        }

        return null
    }

    override fun getGuildContainingMember(uuid: UUID): DataGuild? {
        for (guild in guilds) {
            if (guild.members.contains(uuid)) return guild
        }

        return null
    }

    override fun getGuildsContainingInviteFor(uuid: UUID): HashSet<DataGuild> {
        val invitingGuilds = HashSet<DataGuild>()
        for (guild in guilds) {
            if (guild.invites.contains(uuid)) invitingGuilds.add(guild)
        }

        return invitingGuilds
    }

    override fun addNewGuild(guild: DataGuild): Boolean {
        return guilds.add(guild)
    }

    override fun removeGuild(guild: DataGuild): Boolean {
        return guilds.remove(guild)
    }

    override fun addSAOPlayerData(player: EntityPlayer): SAOPlayerData? {
        return playerData.putIfAbsent(player.uniqueID, SAOPlayerData(player.uniqueID, player.name))
    }

    override fun getSAOPlayerData(uuid: UUID): SAOPlayerData? {
        return playerData.getOrDefault(uuid, null)
    }

    override fun getAllPlayerData(): HashMap<UUID, SAOPlayerData> {
        return playerData
    }

    override fun getCurrentTradingPartner(uuid: UUID): String? {
        return playerData[uuid]?.currentlyTradingWith
    }

    override fun serializeNBT(): NBTTagCompound {
        val nbt = NBTTagCompound()

        val list = NBTTagList()
        for ((key, value) in playerData) {
            val compound = NBTTagCompound()
            compound.setString("uuid", key.toString())
            compound.setTag("nbt", value.serializeNBT())
            list.appendTag(compound)
        }
        nbt.setTag("players", list)

        val list2 = NBTTagList()
        for (guild in guilds) {
            list2.appendTag(guild.serializeNBT())
        }
        nbt.setTag("guilds", list2)

        return nbt
    }

    override fun deserializeNBT(nbt: NBTTagCompound) {

        playerData.clear()
        if (nbt.hasKey("players")) {

            val list = nbt.getTagList("players", Constants.NBT.TAG_COMPOUND)
            for (i in 0 until list.tagCount()) {
                val compound = list.getCompoundTagAt(i)
                if (compound.hasKey("uuid") && compound.hasKey("nbt")) {
                    val uuid = UUID.fromString(compound.getString("uuid"))
                    val data = SAOPlayerData.create(uuid, compound.getCompoundTag("nbt"))
                    playerData[uuid] = data
                }
            }
        }

        guilds.clear()
        if (nbt.hasKey("guilds")) {

            val list = nbt.getTagList("guilds", Constants.NBT.TAG_COMPOUND)
            for (i in 0 until list.tagCount()) {
                val compound = list.getCompoundTagAt(i)
                guilds.add(DataGuild.createGuild(compound))
            }
        }
    }

    override fun hasCapability(capability: Capability<*>, facing: EnumFacing?): Boolean {
        return capability === SAOWorldCap.capability()
    }

    override fun <T> getCapability(capability: Capability<T>, facing: EnumFacing?): T? {
        return if (capability === SAOWorldCap.capability()) SAOWorldCap.capability().cast<T>(this) else null
    }

    companion object {

        fun create(world: World): StandardSAOWorld {
            val wizardryWorld = StandardSAOWorld()
            wizardryWorld.world = world
            return wizardryWorld
        }

        fun create(): StandardSAOWorld {
            return StandardSAOWorld()
        }
    }
}
