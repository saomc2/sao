package com.teamwizardry.sao.api.capability.player;

import com.teamwizardry.sao.SAO;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Objects;

/**
 * Created by Demoniaque on 8/16/2016.
 */
@Mod.EventBusSubscriber(modid = SAO.MOD_ID)
public class SAOPlayerCap implements ICapabilitySerializable<NBTTagCompound> {

	@CapabilityInject(ISAOPlayer.class)
	public static final Capability<ISAOPlayer> CAPABILITY = null;
	private final ISAOPlayer capability;

	public SAOPlayerCap() {
		capability = new DefaultSAOPlayer();
	}

	public SAOPlayerCap(ISAOPlayer capability) {
		this.capability = capability;
	}

	public static Capability<ISAOPlayer> capability() {
		//noinspection ConstantConditions
		return Objects.requireNonNull(CAPABILITY, "CAPABILITY");
	}

	public static ISAOPlayer get(Entity entity) {
		ISAOPlayer cap =  entity.getCapability(CAPABILITY, null);
		if (cap == null) {
			throw new IllegalStateException("Missing capability: " + entity.getName());
		}
		return cap;
	}

	@Override
	public boolean hasCapability(@Nonnull Capability<?> capability, @Nullable EnumFacing facing) {
		return capability == CAPABILITY;
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T> T getCapability(@Nonnull Capability<T> capability, EnumFacing facing) {
		if ((CAPABILITY != null) && (capability == CAPABILITY)) return (T) this.capability;
		return null;
	}

	public static void init() {
		CapabilityManager.INSTANCE.register(ISAOPlayer.class, new SAOPlayerCap.SAOCapabilityStorage(), DefaultSAOPlayer.class);
	}

	@SubscribeEvent(priority = EventPriority.LOWEST)
	public static void attachEntity(AttachCapabilitiesEvent<Entity> e) {
		if (e.getObject() instanceof EntityPlayer) {
			SAOPlayerCap cap = new SAOPlayerCap(new DefaultSAOPlayer((EntityPlayer) e.getObject()));
			e.addCapability(new ResourceLocation(SAO.MOD_ID, "capabilities"), cap);
		}
	}

	@SubscribeEvent
	public static void onLogin(PlayerEvent.PlayerLoggedInEvent event) {
		if (event.player.world.isRemote) return;

		ISAOPlayer cap = SAOPlayerCap.get(event.player);
		if (cap == null) return;
		cap.dataChanged(event.player);
	}

	@SubscribeEvent
	public static void onRespawn(PlayerEvent.PlayerRespawnEvent event) {
		if (event.player.world.isRemote) return;

		ISAOPlayer cap = SAOPlayerCap.get(event.player);
		if (cap == null) return;
		cap.dataChanged(event.player);
	}

	@SubscribeEvent
	public static void onDimChange(PlayerEvent.PlayerChangedDimensionEvent event) {
		if (event.player.world.isRemote) return;

		ISAOPlayer cap = SAOPlayerCap.get(event.player);
		if (cap == null) return;
		cap.dataChanged(event.player);
	}

	@Override
	public NBTTagCompound serializeNBT() {
		return capability.serializeNBT();
	}

	@Override
	public void deserializeNBT(NBTTagCompound nbt) {
		capability.deserializeNBT(nbt);
	}

	public static class SAOCapabilityStorage implements Capability.IStorage<ISAOPlayer> {

		@Override
		public NBTBase writeNBT(Capability<ISAOPlayer> capability, ISAOPlayer instance, EnumFacing side) {
			return instance.serializeNBT();
		}

		@Override
		public void readNBT(Capability<ISAOPlayer> capability, ISAOPlayer instance, EnumFacing side, NBTBase nbt) {
			instance.deserializeNBT((NBTTagCompound) nbt);
		}
	}


}
