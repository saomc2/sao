package com.teamwizardry.sao;

import com.teamwizardry.sao.common.CommonProxy;
import com.teamwizardry.sao.common.command.CommandDuelRequest;
import net.minecraft.launchwrapper.Launch;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import org.apache.logging.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

@Mod(
		modid = SAO.MOD_ID,
		name = SAO.MOD_NAME,
		version = SAO.VERSION,
		dependencies = SAO.DEPENDENCIES
)
public class SAO {

	public static final String MOD_ID = "sao";
	public static final String MOD_NAME = "Sword Art Online";
	public static final String VERSION = "1.0";
	public static final String CLIENT = "com.teamwizardry.sao.client.ClientProxy";
	public static final String SERVER = "com.teamwizardry.sao.common.CommonProxy";
	public static final String DEPENDENCIES = "required-after:librarianlib";
    public static boolean DEV_ENVIRONMENT;
	public static Logger logger;

	@SidedProxy(clientSide = CLIENT, serverSide = SERVER)
	public static CommonProxy proxy;

	/**
	 * This is the instance of your mod as created by Forge. It will never be null.
	 */
	@Mod.Instance(MOD_ID)
	public static SAO instance;

	/**
	 * This is the first initialization event. Register tile entities here.
	 * The registry events below will have fired prior to entry to this method.
	 */
	@Mod.EventHandler
	public void preinit(FMLPreInitializationEvent event) {
		logger = event.getModLog();

		logger.info("Hooking headset...");

		DEV_ENVIRONMENT = (Boolean) Launch.blackboard.get("fml.deobfuscatedEnvironment");

		if(!DEV_ENVIRONMENT){
            GregorianCalendar currentDate = new GregorianCalendar();
            GregorianCalendar expiration = new GregorianCalendar(2019, 11, 30);
            if (currentDate.before(expiration)) {
                logger.info("");
                logger.info(" /---------------------------------------------------------------------------\\");
                logger.info(" |");
                logger.info(" | YOUR TRIAL OF THE SAO MOD EXPIRES ON:");
                logger.info(" |");
                logger.info(" | " + new SimpleDateFormat("dd/MM/yyyy").format(expiration.getTime()));
                logger.info(" |");
                logger.info(" \\---------------------------------------------------------------------------/");
                logger.info("");
                proxy.preInit(event);
            } else {
                logger.info("");
                logger.info(" /---------------------------------------------------------------------------\\");
                logger.info(" |");
                logger.info(" | YOUR TRIAL OF THE SAO MOD EXPIRED ON:");
                logger.info(" |");
                logger.info(" | " + new SimpleDateFormat("dd/MM/yyyy").format(expiration.getTime()));
                logger.info(" |");
                logger.info(" \\---------------------------------------------------------------------------/");
                logger.info("");

                FMLCommonHandler.instance().handleExit(0);
            }
        } else {
            proxy.preInit(event);
        }
	}

	/**
	 * This is the second initialization event. Register custom recipes
	 */
	@Mod.EventHandler
	public void init(FMLInitializationEvent event) {
		proxy.init(event);
	}

	/**
	 * This is the final initialization event. Register actions from other mods here
	 */
	@Mod.EventHandler
	public void postinit(FMLPostInitializationEvent event) {
		proxy.postInit(event);
	}

    @Mod.EventHandler
    //@SideOnly(Side.SERVER)
    public void serverLoad(FMLServerStartingEvent event) {
        event.registerServerCommand(CommandDuelRequest.INSTANCE);
    }
}
